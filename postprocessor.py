###############################################################
# IMPORT MODULES
###############################################################
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy
import glob
import os
import copy
# settings for plot
mpl.use('Agg')
from matplotlib import rc
rc('font',**{'family':'serif','serif':['Helvetica'],'size':'16'})
rc('text',usetex=False)
# create results folder if not there already
if os.path.isdir('results') == 0:
    os.mkdir('results')
#
###############################################################
# DEFINE GLOBAL QUANTITIES
###############################################################
jobpath = 'job'
nvars = 4
ncoords = 3
Wdiam = 4.9999
NWdiam = 3.1*Wdiam
#
###############################################################
# READ UNDEFORMED NODAL COORDINATES
###############################################################
inpfile = '/output_WoundInfliction_REFMECH_Mech.vtk'
copystart = 'POINTS'
copyend = 'CELLS'
#
copyswitch = 0
with open(jobpath + inpfile) as f:
    for line in f:
        if copystart in line:
            linestart = line
            numnodes = int(linestart.split(' ')[1])
            X = np.zeros((numnodes,ncoords))
            inode = 0
            copyswitch = 1
        if copyend in line:
            copyswitch = 0
        if copyswitch == 1 and line != linestart:
            X[inode] = [float((line.split(' ')[j]).split('\n')[0]) for j in range(len(line.split(' ')))]
            inode += 1
#
###############################################################
# READ DISPLACEMENT VALUES
###############################################################
copystart = 'VECTORS u float'
copyend = 'CELL_DATA'
u = np.zeros_like(X)
#
copyswitch = 0
with open(jobpath + inpfile) as f:
    for line in f:
        if copystart in line:
            linestart = line
            inode = 0
            copyswitch = 1
        if copyend in line:
            copyswitch = 0
        if copyswitch == 1 and line != linestart:
            u[inode] = [float((line.split(' ')[j]).split('\n')[0]) for j in range(len(line.split(' ')))]
            inode += 1
#
Xref = X + u
#
# determine nodes corresponding to wound and to its vertices
idxWound = np.where(Xref[:,0]**2.+Xref[:,1]**2.  <= (Wdiam/2.)**2.)
idxNWound = np.where(Xref[:,0]**2.+Xref[:,1]**2. >= (NWdiam/2.)**2.)
#
leftEdgeW = np.intersect1d(np.where(Xref[:,0]==0),idxWound)
idxTopLeftW = leftEdgeW[np.argmax(Xref[leftEdgeW,1])]
leftEdgeNW = np.intersect1d(np.where(Xref[:,0]==0),idxNWound)
idxTopLeftNW = leftEdgeNW[np.argmin(Xref[leftEdgeNW,1])]
#
botEdgeW = np.intersect1d(np.where(Xref[:,1]==0),idxWound)
idxBotRightW = botEdgeW[np.argmax(Xref[botEdgeW,0])]
botEdgeNW = np.intersect1d(np.where(Xref[:,1]==0),idxNWound)
idxBotRightNW = botEdgeNW[np.argmin(Xref[botEdgeNW,0])]
#
###############################################################
# READ SIMULATION OUTPUT - BIOLOGY
###############################################################
allfiles = glob.glob(jobpath + '/*Healing*Biol.vtk')
frameno = []
# print(np.sort(allfiles))
for f in allfiles:
    if f.split('_')[-2][:3] != 'REF':
        frameno += [int(f.split('_')[-2])]
    else:
        idxREF = allfiles.index(f)
#
idxsort = np.argsort(frameno)
#
allfiles_sorted = [allfiles[idxREF]]+[(allfiles[:idxREF] + allfiles[idxREF+1:])[idx] for idx in idxsort]
#
###############################################################
# READ TIME STAMPS
###############################################################
copystart = 'Wound Time'
time = np.zeros(len(allfiles_sorted))
#
for fname in allfiles_sorted:
    iframe = allfiles_sorted.index(fname)
    with open(fname) as f:
        for line in f:
            if copystart in line:
                time[iframe] = int(float(line.split(',')[0][12:]))
#
###############################################################
# READ DISPLACEMENT VALUES
###############################################################
copystart = 'VECTORS u float'
copyend = 'CELL_DATA'
#
# determine wound shape from deformed states
u = np.zeros((len(time),np.shape(X)[0],np.shape(X)[1]))
horzSemiaxis = np.zeros(len(time))
vertSemiaxis = np.zeros(len(time))
Awound = np.zeros(len(time))
for fname in allfiles_sorted:
    iframe = allfiles_sorted.index(fname)
    copyswitch = 0
    with open(fname) as f:
        for line in f:
            if copystart in line:
                linestart = line
                inode = 0
                copyswitch = 1
            if copyend in line:
                copyswitch = 0
            if copyswitch == 1 and line != linestart:
                u[iframe,inode,:] = [float((line.split(' ')[j]).split('\n')[0]) for j in range(len(line.split(' ')))]
                inode += 1
    horzSemiaxis[iframe] = X[idxBotRightW,0] + u[iframe,idxBotRightW,0]
    vertSemiaxis[iframe] = X[idxTopLeftW,1]  + u[iframe,idxTopLeftW,1] 
    Awound[iframe]       = np.pi*horzSemiaxis[iframe]*vertSemiaxis[iframe]
#
Xd0 = X + u[0,:,:]
#
np.savetxt('results/coordsREF_Healing.out',np.c_[Xd0[:,0],Xd0[:,1]])
#
###############################################################
# DISPLAY SIMULATION DOMAIN IN IN VIVO CONFIGURATION
###############################################################
# simulation domain
xmin = np.min(Xd0[:,0])
xmax = np.max(Xd0[:,0])
ymin = np.min(Xd0[:,1])
ymax = np.max(Xd0[:,1])
phivals = np.linspace(0,np.pi/2.)
Rwound = np.mean([Xd0[idxTopLeftW,1],Xd0[idxBotRightW,0]])
RNwound = np.mean([Xd0[idxTopLeftNW,1],Xd0[idxBotRightNW,0]])
Rpunch = Wdiam/2.
#
###############################################################
# READ ALPHA_C_RHO_THETAP VALUES
###############################################################
copystart = 'LOOKUP_TABLE'
copyend = 'VECTORS u float'
alpha_c_rho_thP = np.zeros((np.shape(X)[0],nvars,len(allfiles_sorted)))
#
for fname in allfiles_sorted:
    iframe = allfiles_sorted.index(fname)
    copyswitch = 0
    with open(fname) as f:
        for line in f:
            if copystart in line:
                linestart = line
                inode = 0
                copyswitch = 1
            if copyend in line:
                copyswitch = 0
                break
            if copyswitch == 1 and line != linestart:
                #print line
                alpha_c_rho_thP[inode,:,iframe] = [float((line.split(' ')[j]).split('\n')[0]) for j in range(len(line.split(' ')))]
                inode += 1
    
alpha = alpha_c_rho_thP[:,0,:]
c = alpha_c_rho_thP[:,1,:]
rho = alpha_c_rho_thP[:,2,:]
thetaP = alpha_c_rho_thP[:,3,:]
#
###############################################################
# PLOT ALPHA_C_RHO_THETAP VALUES
###############################################################
# plot time evolution of biological parameters
plt.figure(figsize=(16,4))
#
# time limits and ticks for plotting
tmin = -0.5
tmax = 21.5
tticks_val = [0,3,6,9,12,15,18,21]
tticks_txt = ['0','3','6','9','12','15','18','21']
#
# precursor vs time
plt.subplot(1,4,1)
plt.plot(time/24.,np.mean(alpha[idxWound],axis=0),'-r',label='Wound');
plt.plot(time/24.,np.mean(alpha[idxNWound],axis=0),'--k',label='Far field');
plt.plot([-10,-10],[-10,-10],'ob',label='Data');
plt.legend(loc='upper right',fontsize=13)
plt.xticks(tticks_val,tticks_txt)
plt.xlim(tmin,tmax)
plt.ylim(-0.05,1.05)
plt.xlabel('Time [ days ]')
plt.ylabel(r'Early inflammation: $\alpha$ [ $-$ ]')
plt.grid('on')
#
# chemokines vs time
chemAll_x = np.asarray([1,2,3,4,5,6,7,10,14,15,21])
chemAll_y = np.asarray([[173.2/84.1, 30.7/16.1],\
                       [328.9/145.8],\
                       [1018.9/543.2,342.7/84.1,66.5/16.1],\
                       [273.1/145.8],\
                       [150./84.1],\
                       [70.1/16.1],\
                       [753.7/543.2,161.7/145.8,191.5/84.1],\
                       [142.7/84.1],\
                       [85.4/84.1],\
                       [241.3/145.8],\
                       [124.9/145.8]])
chemAll_avg = [np.mean(chemAll_y[i]) for i in range(len(chemAll_y))]
chemAll_std = [np.std(chemAll_y[i]) for i in range(len(chemAll_y))]
#
plt.subplot(1,4,2)
for i in range(len(chemAll_x)):
    if chemAll_std[i] != 0:
        plt.errorbar(chemAll_x[i],chemAll_avg[i],yerr=chemAll_std[i],linestyle='',color='b',marker='.',markersize=10,capsize=5,capthick=2,label='Data')
    else:
        plt.errorbar(chemAll_x[i],chemAll_avg[i],linestyle='',color='b',marker='.',markersize=10,capsize=5,capthick=2,label='Data')
plt.plot(time/24.,np.mean(c[idxWound],axis=0),'-r',label='Wound');
plt.plot(time/24.,np.mean(c[idxNWound],axis=0),'--k',label='Far field');
plt.xticks(tticks_val,tticks_txt)
plt.xlim(tmin,tmax)
plt.ylim(-0.1,5.1)
plt.xlabel('Time [ days ]')
plt.ylabel('Chemokines: $c$ [ $-$ ]')
plt.grid('on')
#
# cell number vs time
plt.subplot(1,4,3)
plt.plot([2,3,7],[561.6/803.7,516./803.7,3839./936.],'ob',label='Data')
plt.plot(time/24.,np.mean(rho[idxWound],axis=0),'-r',label='Wound');
plt.plot(time/24.,np.mean(rho[idxNWound],axis=0),'--k',label='Far field');
plt.xticks(tticks_val,tticks_txt)
plt.xlim(tmin,tmax)
plt.ylim(-0.1,5.1)
plt.xlabel('Time [ days ]')
plt.ylabel(r'Cells: $\rho$ [ $-$ ]')
plt.grid('on')
#
# permanent deformation vs time
plt.subplot(1,4,4)
plt.plot(time/24.,np.mean(thetaP[idxWound],axis=0),'-r',label='Wound');
plt.plot(time/24.,np.mean(thetaP[idxNWound],axis=0),'--k',label='Far field');
plt.xticks(tticks_val,tticks_txt)
plt.xlim(tmin,tmax)
plt.ylim(0.5,2.5)
plt.xlabel('Time [ days ]')
plt.ylabel(r'Permanent stretch $\theta^p$ [ $-$ ]')
plt.grid('on')
#
plt.tight_layout()
plt.savefig('results/alpha_c_rho_thetaP.pdf',dpi=500)
#
###############################################################
# SAVE P_C_RHO_THETAP VALUES INDIVIDUALLY
###############################################################
#
np.savetxt('results/precursorW.out',np.c_[time/24.,np.mean(alpha[idxWound],axis=0)])
np.savetxt('results/chemicalsW.out',np.c_[time/24.,np.mean(c[idxWound],axis=0)])
np.savetxt('results/cellsW.out',np.c_[time/24.,np.mean(rho[idxWound],axis=0)])
np.savetxt('results/thetaPW.out',np.c_[time/24.,np.mean(thetaP[idxWound],axis=0)])
#
np.savetxt('results/precursorNW.out',np.c_[time/24.,np.mean(alpha[idxNWound],axis=0)])
np.savetxt('results/chemicalsNW.out',np.c_[time/24.,np.mean(c[idxNWound],axis=0)])
np.savetxt('results/cellsNW.out',np.c_[time/24.,np.mean(rho[idxNWound],axis=0)])
np.savetxt('results/thetaPNW.out',np.c_[time/24.,np.mean(thetaP[idxNWound],axis=0)])
#
###############################################################
# READ SIMULATION OUTPUT - MECHANICS
###############################################################
allfiles = glob.glob(jobpath + '/*Healing*Mech.vtk')
frameno = []
# print(np.sort(allfiles))
for f in allfiles:
    if f.split('_')[-2][:3] != 'REF':
        frameno += [int(f.split('_')[-2])]
    else:
        idxREF = allfiles.index(f)
#
idxsort = np.argsort(frameno)
#
allfiles_sorted = [allfiles[idxREF]]+[(allfiles[:idxREF] + allfiles[idxREF+1:])[idx] for idx in idxsort]
#
###############################################################
# READ PHIC_THETA_C10_K1 VALUES
###############################################################
copystart = 'LOOKUP_TABLE'
copyend = 'VECTORS u float'
pc_thE_c10_k1 = np.zeros((np.shape(X)[0],nvars,len(allfiles_sorted)))
#
for fname in allfiles_sorted:
    iframe = allfiles_sorted.index(fname)
    copyswitch = 0
    with open(fname) as f:
        for line in f:
            if copystart in line:
                linestart = line
                inode = 0
                copyswitch = 1
            if copyend in line:
                copyswitch = 0
                break
            if copyswitch == 1 and line != linestart:
                #print line
                pc_thE_c10_k1[inode,:,iframe] = [float((line.split(' ')[j]).split('\n')[0]) for j in range(len(line.split(' ')))]
                inode += 1
#
phic = pc_thE_c10_k1[:,0,:]
thetaE = pc_thE_c10_k1[:,1,:]
C10 = pc_thE_c10_k1[:,2,:]
k1 = pc_thE_c10_k1[:,3,:]
#
###############################################################
# PLOT PHIC_THETAE_C10_K1 VALUES
###############################################################
# plot time evolution of mechanical parameters
plt.figure(figsize=(16,4))
#
# collagen content vs time
allCollSkin = np.asarray([3.75,4.70,1.58,8.94,4.65,2.52,6.63,3.04,8.12])
collAll_x = np.asarray([1,3,5,7,10,14,21])
collAll_y = np.asarray([[2.56],\
                       [3.57,2.96,0.09],\
                       [3.60,4.97,4.34,0.18],\
                       [1.19,4.36,8.77,5.82],\
                       [7.33,5.35],\
                       [6.47],\
                       [7.02]])
collAll_avg = [np.mean(collAll_y[i]) for i in range(len(collAll_y))]
collAll_std = [np.std(collAll_y[i]) for i in range(len(collAll_y))]
#
plt.subplot(1,4,1)
plt.plot(time/24.,np.mean(phic[idxWound],axis=0),'-r',label='Wound');
plt.plot(time/24.,np.mean(phic[idxNWound],axis=0),'--k',label='Far field');
plt.plot([-10,-10],[-10,-10],'ob',label='Data');
plt.legend(loc='lower right',fontsize=13)
for i in range(len(collAll_x)):
    if collAll_std[i] != 0:
        plt.errorbar(collAll_x[i],collAll_avg[i]/np.mean(allCollSkin),yerr=collAll_std[i]/np.mean(allCollSkin),linestyle='',color='b',marker='.',markersize=10,capsize=5,capthick=2,label='Data')
    else:
        plt.errorbar(collAll_x[i],collAll_avg[i]/np.mean(allCollSkin),linestyle='',color='b',marker='.',markersize=10,capsize=5,capthick=2,label='Data')
plt.xticks(tticks_val,tticks_txt)
plt.xlim(tmin,tmax)
plt.ylim(-0.05,2.05)
plt.xlabel('Time [ days ]')
plt.ylabel(r'Collagen content: $\phi_c$ [ $-$ ]')
plt.grid('on')
#
# fibrin content vs time
plt.subplot(1,4,2)
plt.plot(time/24.,np.mean(thetaE[idxWound],axis=0),'-r',label='Wound');
plt.plot(time/24.,np.mean(thetaE[idxNWound],axis=0),'--k',label='Far field');
plt.xticks(tticks_val,tticks_txt)
plt.xlim(tmin,tmax)
plt.ylim(0.6,1.4)
plt.xlabel('Time [ days ]')
plt.ylabel(r'Elastic pre-stretch $\theta^e$ [ $-$ ]')
plt.grid('on')
#
# C10 constitutive parameter vs time
C10_UW  = 1000.*np.array([5.04e-02, 8.25e-05, 4.25e-02, 2.22e-02, 3.43e-03, 2.05e-02, 6.49e-05, 1.21e-04])
C10_7d  = 1000.*np.array([1.81e-02, 3.83e-02, 1.14e-02, 6.46e-02, 8.36e-04, 1.01e-01, 3.36e-02, 3.15e-03])
C10_14d = 1000.*np.array([4.02e-05, 1.68e-03, 1.10e-03, 2.41e-02, 5.83e-05, 8.48e-03, 1.20e-04, 2.50e-05])
#
plt.subplot(1,4,3)
plt.plot(7*np.ones_like(C10_7d)+np.random.normal(0,0.15,len(C10_7d)),C10_7d,'.b',markersize=10,)
plt.plot(14*np.ones_like(C10_14d)+np.random.normal(0,0.15,len(C10_14d)),C10_14d,'.b',markersize=10,)
plt.plot(time/24.,1000.*np.mean(C10[idxWound],axis=0),'-r',label='Wound');
plt.plot(time/24.,1000.*np.mean(C10[idxNWound],axis=0),'--k',label='Far field');
plt.xticks(tticks_val,tticks_txt)
plt.xlim(tmin,tmax)
plt.ylim(0.01,200)
plt.xlabel('Time [ days ]')
plt.ylabel('Mechanical param. $C_{10}$ [ kPa ]')
plt.grid('on')
plt.yscale('log')
#
# k1 constitutive parameter vs time
k1_UW  = np.array([1.55, 1.14, 2.99, 2.44, 1.24, 1.64, 2.02, 0.29])
k1_7d  = np.array([86.54, 2.18, 0.50, 45.75, 84.72, 33.86, 1.20, 4.04])
k1_14d = np.array([6.70, 1.16, 1.47, 11.21, 18.87, 21.89, 174.85, 3.03])
#
plt.subplot(1,4,4)
plt.plot(7*np.ones_like(k1_7d)+np.random.normal(0,0.15,len(k1_7d)),k1_7d,'.b',markersize=10,)
plt.plot(14*np.ones_like(k1_14d)+np.random.normal(0,0.15,len(k1_14d)),k1_14d,'.b',markersize=10,)
plt.plot(time/24.,np.mean(k1[idxWound],axis=0),'-r',label='Wound');
plt.plot(time/24.,np.mean(k1[idxNWound],axis=0),'--k',label='Far field');
plt.xticks(tticks_val,tticks_txt)
plt.xlim(tmin,tmax)
plt.ylim(0.01,250)
plt.xlabel('Time [ days ]')
plt.ylabel('Mechanical param. $k_1$ [ MPa ]')
plt.grid('on')
plt.yscale('log')
#
plt.tight_layout()
plt.savefig('results/phic_thetaE_C10_k1.pdf',dpi=500)
#
###############################################################
# SAVE PHIC_THETAE_C10_K1 VALUES INDIVIDUALLY
###############################################################
#
np.savetxt('results/collagenW.out',np.c_[time/24.,np.mean(phic[idxWound],axis=0)])
np.savetxt('results/thetaEW.out',np.c_[time/24.,np.mean(thetaE[idxWound],axis=0)])
np.savetxt('results/C10W.out',np.c_[time/24.,np.mean(C10[idxWound],axis=0)])
np.savetxt('results/k1W.out',np.c_[time/24.,np.mean(k1[idxWound],axis=0)])
#
np.savetxt('results/collagenNW.out',np.c_[time/24.,np.mean(phic[idxNWound],axis=0)])
np.savetxt('results/thetaENW.out',np.c_[time/24.,np.mean(thetaE[idxNWound],axis=0)])
np.savetxt('results/C10NW.out',np.c_[time/24.,np.mean(C10[idxNWound],axis=0)])
np.savetxt('results/k1NW.out',np.c_[time/24.,np.mean(k1[idxNWound],axis=0)])
#
###############################################################
# PLOT WOUND AREA VALUES
###############################################################
# plot time evolution of wound area
plt.figure(figsize=(4,4))
#
Apunch = np.pi/4.*Wdiam**2
time_exp = [3,5,7,10,14,21]
Awound_avg_norm = np.array([1., 0.60574798, 0.4204466, 0.32083419, 0.34105053, 0.52468416])
Awound_std_norm = np.array([np.nan, 0.17823166, 0.10988036, 0.06725248, 0.103004 ,0.13791739])
# NOTE: the 1st std value is in fact finite but we set it to nan to make python skip it
# this is because there's little value in showing std for a value normalized to itself
# 
idxstart = np.argmin(np.abs(time-3.*24.))
plt.errorbar(time_exp,Awound_avg_norm,yerr=Awound_std_norm,linestyle='--',color='b',marker='.',markersize=10,capsize=5,capthick=2,label='Data')
plt.plot(time[idxstart:]/24.,Awound[idxstart:]/Awound[idxstart],'-r');
plt.hlines(Apunch,-1,25,linestyles='--',color='k',label='Biopsy punch size')
plt.xticks(tticks_val,tticks_txt)
plt.xlim(2.5,tmax)
plt.ylim(0,1.05)
plt.xlabel('Time [ days ]')
plt.ylabel(r'Wound area / d3 area [ $-$ ]')
plt.grid('on')
#
plt.tight_layout()
plt.savefig('results/Awound.pdf',dpi=500)
#
np.savetxt('results/areaW.out',np.c_[time/24.,Awound])

