/*
	Pre and Post processing functions
	Solver functions
	Struct and classes for the problem definition
*/

#ifndef solverTri_h
#define solverTri_h
#include "tissue_tri.h"
#include <vector>
#include <map>
#include <Eigen/Dense> // most of the vector functions I will need inside of an element
using namespace Eigen;



//-------------------------------------------------//
// PRE PROCESS
//-------------------------------------------------//

//----------------------------//
// FILL DOF
//----------------------------//
//
// now I have the mesh and
// =>> Somehow filled in the essential boundary conditions
// so I create the dof maps.
void fillDOFmap(tissue &myTissue);

//----------------------------//
// EVAL JAC and FF
//----------------------------//
//
// eval the jacobians that I use later on in the element subroutines
void evalElemJacobians(tissue &myTissue);
void evalElemStressStrain(tissue &myTissue);

//-------------------------------------------------//
// SOLVER
//-------------------------------------------------//

//----------------------------//
// SPARSE SOLVER
//----------------------------//
//
void sparseWoundSolver(tissue &myTissue, std::string filename, double save_freq,const std::vector<int> &save_node,const std::vector<int> &save_ip);
void sparseLoadSolver(tissue &myTissue, std::string filename);
void equilMechanics(tissue &myTissue, std::string filename);
// dense solver in previous version of wound.cpp
// void denseWoundSolver(tissue &myTissue, std::string filename, int save_freq);



#endif
