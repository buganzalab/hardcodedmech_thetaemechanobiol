##=================##
## MAKE FILE
##=================##

##—————————————————##
## flags
##—————————————————##
CC = g++ 
INCPATH = -I/home/mpensalf/wound_code_fixed_MechaLink/include -Iinclude -I/home/mpensalf -I/apps/cent7/boost/1.64.0_gcc-5.2.0/include
LIBPATH = -L /apps/cent7/gcc/6.3.0/lib
CFLAGS = -std=c++11 -O3

##—————————————————##
## rule to make all
##—————————————————##
%.o: %.cpp
	$(CC) -c $(INCPATH) $(CFLAGS) $<

##—————————————————##
## core code
##—————————————————##
objs/biology.o: src/biology.cpp | objs
	$(CC) $(INCPATH) $(LIBPATH) -c $(CFLAGS) src/biology.cpp
	mv biology.o objs

objs/mechanics.o: src/mechanics.cpp | objs
	$(CC) $(INCPATH) $(LIBPATH) -c $(CFLAGS) src/mechanics.cpp
	mv mechanics.o objs

objs/wound_tri.o: src/wound_tri.cpp | objs
	$(CC) $(INCPATH) $(LIBPATH) -c $(CFLAGS) src/wound_tri.cpp
	mv wound_tri.o objs

objs/utilities_tri.o: src/utilities_tri.cpp | objs
	$(CC) $(INCPATH) $(LIBPATH) -c $(CFLAGS) src/utilities_tri.cpp
	mv utilities_tri.o objs

objs/solver_tri.o: src/solver_tri.cpp | objs
	$(CC) $(INCPATH) $(LIBPATH) -c $(CFLAGS) src/solver_tri.cpp
	mv solver_tri.o objs

objs:
	mkdir -p objs

##—————————————————##
## tests
##—————————————————##
apps/test_biology: apps/test_biology.cpp objs/biology.o
	$(CC) $(INCPATH) $(LIBPATH) $(CFLAGS) $^ -o test_biology
	mv test_biology apps
apps/test_localWound: apps/test_localWound.cpp objs/biology.o objs/mechanics.o objs/wound.o
	$(CC) $(INCPATH) $(LIBPATH) $(CFLAGS) $^ -o test_localWound
	mv test_localWound apps
apps/test_elementWound: apps/test_elementWound.cpp objs/biology.o objs/mechanics.o objs/wound.o
	$(CC) $(INCPATH) $(LIBPATH) $(CFLAGS) $^ -o test_elementWound
	mv test_elementWound apps
apps/test_one_element: apps/test_one_element.cpp objs/biology.o objs/mechanics.o objs/wound.o objs/solver.o
	$(CC) $(INCPATH) $(LIBPATH) $(CFLAGS) $^ -o test_one_element
	mv test_one_element apps

##—————————————————##
## results, full
##—————————————————##
apps/sim_wound_tri_sym: apps/sim_wound_tri_sym.cpp objs/biology.o objs/mechanics.o objs/wound_tri.o objs/utilities_tri.o objs/solver_tri.o
		$(CC) $(INCPATH) $(LIBPATH) $(CFLAGS) $^ -o sim_wound_tri_sym
		mv sim_wound_tri_sym apps

##—————————————————##
## clean and remove
##—————————————————##

clean:
	rm $(wildcard objs/*.o)
