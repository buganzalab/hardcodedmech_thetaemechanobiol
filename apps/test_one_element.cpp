/*

RESULTS
circular wound problem.

Read a quad mesh defined from Comsol, and edited by me.
Then apply boundary conditions and initial conditions. 
Solve. 

*/

#include "wound.h"
#include "solver.h"
//#include "myMeshGenerator.h"

#include <vector>
#include <iostream>
#include <sstream>
#include <fstream>
#include <stdexcept> 
#include <math.h> 
#include <string>
#include <time.h>

#include <Eigen/Dense> 
using namespace Eigen;

double frand(double fMin, double fMax)
{
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}


int main(int argc, char *argv[])
{

	std::cout<<"\nResults: full domain simulations for cell migration proliferation\n";
	srand (time(NULL));
	
	//---------------------------------//
	// GLOBAL PARAMETERS
	//
	// for normalization
	double rho_phys = 300.; // [cells/mm^3]
	double c_max = 1.0e-4; // [g/mm3] from tgf beta review, 5e-5g/mm3 was good for tissues
	//
	double k0 = 1.0; // neo hookean for skin, used previously, in kPa
	double k2c = 5.79; // stiffness of collagen in MPa, from previous paper
	double k2f = 5.52; // nonlinear exponential coefficient, non-dimensional
	// not for now
	double t_rho = 0.0; // force of fibroblasts in MPa, this is per cell. so, in an average sense this is the production by the natural density
	double t_rho_c = 0.0; // force of myofibroblasts enhanced by chemical, I'm assuming normalized chemical, otherwise I'd have to add a normalizing constant
	double K_t_c = c_max/10.; // saturation of chemical on force. this can be calculated from steady state
	double D_rhorho = 0.0833; // diffusion of cells in [mm^2/hour], not normalized
	double D_rhoc = 1.66e-12/c_max/c_max; // diffusion of chemotactic gradient, an order of magnitude greater than random walk [mm^2/hour], not normalized
	//
	double D_cc = 0.10; // diffusion of chemical TGF, not normalized. 
	// not for now
	double p_rho =0.0; // in 1/hour production of fibroblasts naturally, proliferation rate, not normalized, based on data of doubling rate from commercial use
	double p_rho_c = 0.0; // production enhanced by the chem, if the chemical is normalized, then suggest two fold,
	double p_rho_theta = 0.0; // enhanced production by theta
	double K_rho_c= c_max/10.; // saturation of cell proliferation by chemical, this one is definitely not crucial, just has to be small enough <cmax
	double d_rho = 0.2*p_rho ;// decay of cells, 20 percent of cells die per day
	double K_rho_rho = 361.0; // saturation of cell by cell, from steady state
	double vartheta_e = 2.0; // physiological state of area stretch
	double gamma_theta = 5.; // sensitivity of heviside function
	// not now
	double p_c_rho = 0*90.0e-16/rho_phys;// production of c by cells in g/cells/h
	double p_c_thetaE = 300.0e-16/rho_phys; // coupling of elastic and chemical, three fold
	double K_c_c = 1.;// saturation of chem by chem, from steady state
	double d_c = 0.01; // decay of chemical in 1/hours
	//---------------------------------//
	std::vector<double> global_parameters = {k0,k2c,k2f,t_rho,t_rho_c,K_t_c,D_rhorho,D_rhoc,D_cc,p_rho,p_rho_c,p_rho_theta,K_rho_c,K_rho_rho,d_rho,vartheta_e,gamma_theta,p_c_rho,p_c_thetaE,K_c_c,d_c};

	//---------------------------------//
	// LOCAL PARAMETERS
	//
	// fiber alignment
	double tau_omega_c = 100; // time constant for angular reorientation of collagen
	double tau_omega_f = 100; // time constant for angular reorientation of fibronectin
	//
	// dispersion parameter
	double tau_kappa_c   = 10; // time constant
	double gamma_kappa_c = 5; // exponent of the principal stretch ratio
	double tau_kappa_f   = 10; // time constant
	double gamma_kappa_f = 5; // exponent of the principal stretch ratio
	// 
	// permanent contracture/growth
	double tau_lamdaP_a = 10; // time constant for direction a
	double tau_lamdaP_s = 10; // time constant for direction s
	//
	double thick_0  =  5; // reference thickness in mm
	double phic_vol =  0.5; // volume of collagen associated to healthy collagen volume fraction.
	double phif_vol =  0.01; // volume ot fibronectin associated to healt
	//
	std::vector<double> local_parameters = {tau_omega_c,tau_omega_f,tau_kappa_c,gamma_kappa_c,tau_kappa_f,gamma_kappa_f,tau_lamdaP_a,tau_lamdaP_s,thick_0,phic_vol,phif_vol};	
	//
	//
	// other local stuff
	double PIE = 3.14159;
	//---------------------------------//
	
	
	//---------------------------------//
	// values for the wound
	double rho_wound = 0; // [cells/mm^3]
	double c_wound = 0.0e-4;
	double phic0_wound=0.5;
	double kc0_wound = 1180.; // kPa
	double phif0_wound=0.01;
	double kf0_wound = 10.; // kPa
	double kappac0_wound = 0.5;
	double kappaf0_wound = 0.5;
	double a0y = frand(0.9,1.);
	double a0x = sqrt(1-a0y*a0y);
	Vector2d a0c_wound;a0c_wound<<a0x,a0y;
	Vector2d a0f_wound;a0f_wound<<a0x,a0y;
	Vector2d lamda0_wound;lamda0_wound<<1.,1.;
	//---------------------------------//
	
	
	//---------------------------------//
	// values for the healthy
	double rho_healthy = 300; // [cells/mm^3]
	double c_healthy = 0.0;
	double phic0_healthy=0.5;
	double kc0_healthy = 2180.; // kPa
	double phif0_healthy=0.01;
	double kf0_healthy = 99.4; // kPa
	double kappac0_healthy = 0.42;
	double kappaf0_healthy = 0.5;
	Vector2d a0c_healthy;a0c_healthy<<0.0,1.0;
	Vector2d a0f_healthy;a0f_healthy<<0.0,1.0;
	Vector2d lamda0_healthy;lamda0_healthy<<1.,1.;
	//---------------------------------//
	
	
	//---------------------------------//
	// create mesh (only nodes and elements)
	std::cout<<"Going to read the mesh from file: "<<argv[1]<<"\n";
	tissue myTissue;
	readComsolEditInput(argv[1],myTissue);
	std::cout<<"Read in mesh with: "<<myTissue.n_node<<" nodes, "<<myTissue.n_quadri<<" elements\n";
	int n_node = myTissue.n_node;
	int n_elem = myTissue.n_quadri;
	// and initialize the rest of the fields in the struct and send to run a single step
	//
	// global fields rho and c initial conditions 
	std::vector<double> node_rho0(n_node,rho_healthy);
	std::vector<double> node_c0 (n_node,c_healthy);
	//
	// values at the integration points
	std::vector<double> ip_phic0(n_elem*4,phic0_healthy);
	std::vector<double> ip_kc0(n_elem*4,kc0_healthy);
	std::vector<Vector2d> ip_a0c0(n_elem*4,a0c_healthy);
	std::vector<double> ip_kappac0(n_elem*4,kappac0_healthy);
	//
	std::vector<double> ip_phif0(n_elem*4,phif0_healthy);
	std::vector<double> ip_kf0(n_elem*4,kf0_healthy);
	std::vector<Vector2d> ip_a0f0(n_elem*4,a0f_healthy);
	std::vector<double> ip_kappaf0(n_elem*4,kappaf0_healthy);
	//
	std::vector<Vector2d> ip_lamda0(n_elem*4,lamda0_healthy);
	//
	// define ellipse
	double x_center = 0.;
	double y_center = 0.;
	double x_axis = 4.;
	double y_axis = 4.;
	double alpha_ellipse = 0.;
	// boundary conditions and definition of the wound
	double tol_boundary = 1e-5;
	std::map<int,double> eBC_x;
	std::map<int,double> eBC_rho;
	std::map<int,double> eBC_c;
	for(int nodei=0;nodei<n_node;nodei++){
		double x_coord = myTissue.node_X[nodei](0);
		double y_coord = myTissue.node_X[nodei](1);
		// check if this node is close to the edge of the mesh
		if(x_coord<-24.9 || x_coord>24.9 || y_coord<-24.9 || y_coord>24.9){
			// insert the boundary condition for displacement
			std::cout<<"fixing node "<<nodei<<"\n";
			eBC_x.insert ( std::pair<int,double>(nodei*2+0,myTissue.node_X[nodei](0)) ); // x coordinate
			eBC_x.insert ( std::pair<int,double>(nodei*2+1,myTissue.node_X[nodei](1)) ); // y coordinate 
			// insert the boundary condition for rho
			eBC_rho.insert ( std::pair<int,double>(nodei,rho_healthy) ); 
			// insert the boundary condition for c
			eBC_c.insert   ( std::pair<int,double>(nodei,c_healthy) );
		}else{
			eBC_x.insert ( std::pair<int,double>(nodei*2+0,myTissue.node_X[nodei](0)) ); // x coordinate
			eBC_x.insert ( std::pair<int,double>(nodei*2+1,myTissue.node_X[nodei](1)) ); // y coordinate 
			eBC_c.insert   ( std::pair<int,double>(nodei,c_healthy) );
		}
		// check if it is in the center of the wound
		double check_ellipse = pow((x_coord-x_center)*cos(alpha_ellipse)+(y_coord-y_center)*sin(alpha_ellipse),2)/(x_axis*x_axis) +\
						pow((x_coord-x_center)*sin(alpha_ellipse)+(y_coord-y_center)*cos(alpha_ellipse),2)/(y_axis*y_axis) ;
		if(check_ellipse<=1.1){
			// inside
			std::cout<<"wound node "<<nodei<<"\n";
			node_rho0[nodei] = rho_wound;
			node_c0[nodei] = c_wound;
		}
	}
	// check wounds for the elements, to adjust the structural variables
	for(int elemi=0;elemi<n_elem;elemi++){
		std::vector<Vector3d> IP = LineQuadriIP();
		for(int ip=0;ip<IP.size();ip++)
		{
			double xi = IP[ip](0);
			double eta = IP[ip](1);
			// weight of the integration point
			double wip = IP[ip](2);
			std::vector<double> R = evalShapeFunctionsR(xi,eta);
			Vector2d X_IP;X_IP.setZero();
			for(int nodej=0;nodej<4;nodej++){
				X_IP += R[nodej]*myTissue.node_X[myTissue.LineQuadri[elemi][nodej]];
			}
			double check_ellipse_ip = pow((X_IP(0)-x_center)*cos(alpha_ellipse)+(X_IP(1)-y_center)*sin(alpha_ellipse),2)/(x_axis*x_axis) +\
						pow((X_IP(0)-x_center)*sin(alpha_ellipse)+(X_IP(1)-y_center)*cos(alpha_ellipse),2)/(y_axis*y_axis) ;
			if(check_ellipse_ip<=1.1){
				std::cout<<"wound IP node: "<<4*elemi+ip<<"\n";
				//
				ip_phic0[elemi*4+ip] = phic0_wound;
				ip_kc0[elemi*4+ip] = kc0_wound;
				ip_a0c0[elemi*4+ip] = a0c_wound;
				ip_kappac0[elemi*4+ip] = kappac0_wound;
				//
				ip_phif0[elemi*4+ip] = phif0_wound;
				ip_kf0[elemi*4+ip] = kf0_wound;
				ip_a0f0[elemi*4+ip] = a0f_wound;
				ip_kappaf0[elemi*4+ip] = kappaf0_wound;
				//
				ip_lamda0[elemi*4+ip] = lamda0_wound;
			}
		}
	}
	// no neumann boundary conditions. 
	std::map<int,double> nBC_x;
	std::map<int,double> nBC_rho;
	std::map<int,double> nBC_c;
	
	// parameters
	myTissue.global_parameters = global_parameters;
	myTissue.local_parameters = local_parameters;
	//
	myTissue.node_x = myTissue.node_X;
	myTissue.node_rho_0 = node_rho0;
	myTissue.node_rho = node_rho0;
	myTissue.node_c_0 = node_c0;
	myTissue.node_c = node_c0;
	//
	myTissue.ip_phic_0 = ip_phic0;	
	myTissue.ip_phic = ip_phic0;	
	myTissue.ip_kc_0 = ip_kc0;	
	myTissue.ip_kc = ip_kc0;	
	myTissue.ip_a0c_0 = ip_a0c0;		
	myTissue.ip_a0c = ip_a0c0;	
	myTissue.ip_kappac_0 = ip_kappac0;	
	myTissue.ip_kappac = ip_kappac0;	
	//
	myTissue.ip_phif_0 = ip_phif0;	
	myTissue.ip_phif = ip_phif0;
	myTissue.ip_kf_0 = ip_kf0;	
	myTissue.ip_kf = ip_kf0;		
	myTissue.ip_a0f_0 = ip_a0f0;		
	myTissue.ip_a0f = ip_a0f0;	
	myTissue.ip_kappaf_0 = ip_kappaf0;	
	myTissue.ip_kappaf = ip_kappaf0;	
	//
	myTissue.ip_lamdaP_0 = ip_lamda0;	
	myTissue.ip_lamdaP = ip_lamda0;	
	//
	myTissue.eBC_x = eBC_x;
	myTissue.eBC_rho = eBC_rho;
	myTissue.eBC_c = eBC_c;
	myTissue.nBC_x = nBC_x;
	myTissue.nBC_rho = nBC_rho;
	myTissue.nBC_c = nBC_c;
	myTissue.time_final = 24.0*16;
	myTissue.time_step = 0.25;
	myTissue.local_time_step = 1.0;
	myTissue.tol = 1e-8;
	myTissue.max_iter = 20;
	myTissue.n_IP = 4*n_elem;
	//
	std::cout<<"filling dofs...\n";
	fillDOFmap(myTissue);
	std::cout<<"going to eval jacobians...\n";
	evalElemJacobians(myTissue);
	//
	//print out the Jacobians
	
	std::cout<<"element jacobians\nJacobians= ";
	std::cout<<myTissue.elem_jac_IP.size()<<"\n";
	for(int i=0;i<myTissue.elem_jac_IP.size();i++){
		std::cout<<"element: "<<i<<"\n";
		for(int j=0;j<4;j++){
			std::cout<<"ip; "<<i<<"\n"<<myTissue.elem_jac_IP[i][j]<<"\n";
		}
	}
	// print out the forward dof map
	std::cout<<"Total :"<<myTissue.n_dof<<" dof\n";
	for(int i=0;i<myTissue.dof_fwd_map_x.size();i++){
		std::cout<<"x node*2+coord: "<<i<<", dof: "<<myTissue.dof_fwd_map_x[i]<<"\n";
	}
	for(int i=0;i<myTissue.dof_fwd_map_rho.size();i++){
		std::cout<<"rho node: "<<i<<", dof: "<<myTissue.dof_fwd_map_rho[i]<<"\n";
	}
	for(int i=0;i<myTissue.dof_fwd_map_c.size();i++){
		std::cout<<"c node: "<<i<<", dof: "<<myTissue.dof_fwd_map_c[i]<<"\n";
	}
	
	//
	// 
	std::cout<<"going to start solver\n";
	// save a node and an integration point to a file
	std::vector<int> save_node;save_node.clear();save_node.push_back(4);
	std::vector<int> save_ip;save_ip.clear();
	
		
	std::stringstream ss;
	std::string filename = "data/4elem_wound"+ss.str()+"_";
	
	
	
	//----------------------------------------------------------//
	// SOLVE
	sparseWoundSolver(myTissue, filename, 100,save_node,save_ip);
	//----------------------------------------------------------//
	
		
	return 0;	
}