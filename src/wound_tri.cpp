/*

WOUND

This code is the implementation of the DaLaWoHe

*/
//
#include "wound_tri.h"
#include "biology.h"
#include "mechanics.h"
#include <iostream>
#include <math.h>
#include <stdexcept>
#include <iostream>
#include <Eigen/Dense> // most of the vector functions I will need inside of an element
using namespace Eigen;

//--------------------------------------------------------//
// RESIDUAL AND TANGENT
//--------------------------------------------------------//

// ELEMENT RESIDUAL AND TANGENT
void evalWound(
	double dt,double local_dt,
	const Matrix2d &ip_Jac,
	const std::vector<double> &global_parameters,const std::vector<double> &local_parameters,
	const std::vector<double> &node_rho_0, const std::vector<double> &node_c_0,const std::vector<double> &node_p_0,
	const double &ip_phic_0,const double &ip_mu_0,const double &ip_kc_0,const Vector2d &ip_a0c_0,const double &ip_kappac_0,
	const double &ip_phif_0,const double &ip_kf_0,const Vector2d &ip_a0f_0,const double &ip_kappaf_0,
	const Vector2d &ip_lamdaP_0,
	const std::vector<double> &node_rho, const std::vector<double> &node_c,
	double &ip_phic, double &ip_mu, double &ip_kc, Vector2d &ip_a0c, double &ip_kappac,
	double &ip_phif, double &ip_kf, Vector2d &ip_a0f, double &ip_kappaf,
	Vector2d &ip_lamdaP, Matrix2d &ip_FF,
	const std::vector<Vector2d> &node_x,
	VectorXd &Re_x,MatrixXd &Ke_x_x,MatrixXd &Ke_x_rho,MatrixXd &Ke_x_c,
	VectorXd &Re_rho,MatrixXd &Ke_rho_x, MatrixXd &Ke_rho_rho,MatrixXd &Ke_rho_c,
	VectorXd &Re_c,MatrixXd &Ke_c_x,MatrixXd &Ke_c_rho,MatrixXd &Ke_c_c)
{

	//---------------------------------//
	// INPUT
	//  dt: time step
	//  local_dt: local time step for remodeling
	//	elem_jac_IP: jacobians at the integration points, needed for the deformation grad
	//  global_param: material parameters for global newton
	//  local_param: material parameters for local newton
	//  Xi_t: global fields at previous time step
	//  Theta_t: structural fields at previous time steps
	//  Xi: current guess of the global fields
	//  Theta: current guess of the structural fields
	//	node_x: deformed positions
	//
	// OUTPUT
	//  Re: all residuals
	//  Ke: all tangents
	//
	// Algorithm
	//  0. Loop over integration points
	//	1. F,rho,c,nabla_rho,nabla_c: deformation at IP
	//  2. LOCAL NEWTON -> update the current guess of the structural parameters
	//  3. Fe,Fp
	//	4. Se_pas,Se_act,S
	//	5. Qrho,Srho,Qc,Sc
	//  6. Residuals
	//  7. Tangents
	//---------------------------------//



	//---------------------------------//
	// GLOBAL PARAMETERS
	//
	// double k0 = global_parameters[0]; // neo hookean
	double k2c = global_parameters[1]; // nonlinear exponential collagen
	double k2f = global_parameters[2]; // nonlinear exponential fibronectin
	double t_rho = global_parameters[3]; // force of fibroblasts
	double t_rho_c = global_parameters[4]; // force of myofibroblasts enhanced by chemical
	double K_t_c = global_parameters[5]; // saturation of chemical on force
	double D_rhorho = global_parameters[6]; // diffusion of cells
	double D_rhoc = global_parameters[7]; // diffusion of chemotactic gradient
	double Fc_max = global_parameters[8];
	double D_cc = global_parameters[9]; // diffusion of chemical
	double p_rho =global_parameters[10]; // production of fibroblasts naturally
	double p_rho_c = global_parameters[11]; // production enhanced by the chem
	double p_rho_theta = global_parameters[12]; // mechanosensing
	double K_rho_c= global_parameters[13]; // saturation of cell production by chemical
	double K_rho_rho = global_parameters[14]; // saturation of cell by cell
	double d_rho = global_parameters[15] ;// decay of cells
	double theta_phy = global_parameters[16]; // physiological state of area stretch
	double gamma_c_thetaE = global_parameters[17]; // sensitivity of heviside function
	double p_c_rho = global_parameters[18];// production of C by cells
	double p_c_thetaE = global_parameters[19]; // coupling of elastic and chemical
	double K_c_c = global_parameters[20];// saturation of chem by chem
	double d_c = global_parameters[21]; // decay of chemical
	double thick_0 = global_parameters[22]; // thickness in reference
	double D_precprec = global_parameters[23]; // diffusion of neutrophil/pdgf
	double d_prec = global_parameters[24]; // decay of neutrophil/pdgf
	double p_c_prec = global_parameters[25]; // production of TGFb1/Macrophage by neutrophil/PDGF
	double prec_healthy =  global_parameters[26]; // healthy level of precursor 
	double c_target =  global_parameters[27]; // target level of TGFb1 to ensure that healthy value = 1
	double rho_healthy =  global_parameters[28]; // healthy level of rho 
	//
	//
	//---------------------------------//



	//---------------------------------//
	// GLOBAL VARIABLES
	// Initialize the residuals to zero and declare some global stuff
	Re_x.setZero();
	Re_rho.setZero();
	Re_c.setZero();
	Ke_x_x.setZero();
	Ke_x_rho.setZero();
	Ke_x_c.setZero();
	Ke_rho_x.setZero();
	Ke_rho_rho.setZero();
	Ke_rho_c.setZero();
	Ke_c_x.setZero();
	Ke_c_rho.setZero();
	Ke_c_c.setZero();
	int n_nodes = 3;
	std::vector<Vector2d> Ebasis; Ebasis.clear();
	Matrix2d Rot90;Rot90<<0.,-1.,1.,0.;
	Ebasis.push_back(Vector2d(1.,0.)); Ebasis.push_back(Vector2d(0.,1.));
	double load = 1.0;
	//---------------------------------//



	//--------------------------------------//
	// LOOP OVER INTEGRATION POINTS, only 1!
	//--------------------------------------//

	// array with integration points
	// shape function and derivatives
    double xi  = 1./3.;
    double eta = 1./3.;
    std::vector<double> R   ={1.-xi-eta,xi,eta};
    std::vector<double> Rxi ={-1.,1.,0.};
    std::vector<double> Reta={-1.,0.,1.};
	double Jac = 1./ip_Jac.determinant();

	// declare variables and gradients at IP
	std::vector<Vector2d> dRdXi;dRdXi.clear();
	Vector2d dxdxi,dxdeta;
	dxdxi.setZero();dxdeta.setZero();
	double rho_0=0.; Vector2d drho0dXi; drho0dXi.setZero();
	double rho=0.; Vector2d drhodXi; drhodXi.setZero();
	double c_0=0.; Vector2d dc0dXi; dc0dXi.setZero();
	double c=0.; Vector2d dcdXi; dcdXi.setZero();
	double prec=0.;
	//
	for(int ni=0;ni<n_nodes;ni++)
	{
		dRdXi.push_back(Vector2d(Rxi[ni],Reta[ni]));

		dxdxi += node_x[ni]*Rxi[ni];
		dxdeta += node_x[ni]*Reta[ni];

		rho_0 += node_rho_0[ni]*R[ni];
		drho0dXi(0) += node_rho_0[ni]*Rxi[ni];
		drho0dXi(1) += node_rho_0[ni]*Reta[ni];

		rho += node_rho[ni]*R[ni];
		drhodXi(0) += node_rho[ni]*Rxi[ni];
		drhodXi(1) += node_rho[ni]*Reta[ni];

		c_0 += node_c_0[ni]*R[ni];
		dc0dXi(0) += node_c_0[ni]*Rxi[ni];
		dc0dXi(1) += node_c_0[ni]*Reta[ni];

		c += node_c[ni]*R[ni];
		dcdXi(0) += node_c[ni]*Rxi[ni];
		dcdXi(1) += node_c[ni]*Reta[ni];

		prec += node_p_0[ni]*R[ni];
	}
	//
	//---------------------------------//



	//---------------------------------//
	// EVAL GRADIENTS
	//---------------------------------//
	//
	// Deformation gradient and strain
	// assemble the columns
	Matrix2d dxdXi; dxdXi<<dxdxi(0),dxdeta(0),dxdxi(1),dxdeta(1);
	// F = dxdX
	Matrix2d FF = dxdXi*ip_Jac.transpose();
	// FF = Matrix2d::Identity(2,2);
	// STORE deformation gradient tensor in the output
	ip_FF = FF;
	// the strain
	Matrix2d Identity;Identity<<1.,0.,0.,1.;
	// Matrix2d EE = 0.5*(FF.transpose()*FF - Identity);
	Matrix2d CC = FF.transpose()*FF;
	Matrix2d CCinv = CC.inverse();
	//
	// Gradient of concentrations in current configuration
	Matrix2d dXidx = dxdXi.inverse();
	Vector2d grad_rho0 = dXidx.transpose()*drho0dXi;
	Vector2d grad_rho  = dXidx.transpose()*drhodXi;
	Vector2d grad_c0   = dXidx.transpose()*dc0dXi;
	Vector2d grad_c    = dXidx.transpose()*dcdXi;
	//
	// Gradient of concentrations in reference
	Vector2d Grad_rho0 = ip_Jac*drho0dXi;
	Vector2d Grad_rho = ip_Jac*drhodXi;
	Vector2d Grad_c0 = ip_Jac*dc0dXi;
	Vector2d Grad_c = ip_Jac*dcdXi;
	//
	// Gradient of basis functions for the nodes in reference
	std::vector<Vector2d> Grad_R;Grad_R.clear();
	Grad_R.push_back(ip_Jac*dRdXi[0]);
	Grad_R.push_back(ip_Jac*dRdXi[1]);
	Grad_R.push_back(ip_Jac*dRdXi[2]);
	//
	// Gradient of basis functions in deformed configuration
	std::vector<Vector2d> grad_R;grad_R.clear();
	grad_R.push_back(dXidx.transpose()*dRdXi[0]);
	grad_R.push_back(dXidx.transpose()*dRdXi[1]);
	grad_R.push_back(dXidx.transpose()*dRdXi[2]);
	//
	//---------------------------------//

	//---------------------------------//
	// LOCAL PROBLEM: structural update
	//
	VectorXd dThetadCC(12*4);dThetadCC.setZero();
	VectorXd dThetadrho(12);dThetadrho.setZero();
	VectorXd dThetadc(12);dThetadc.setZero();
	//*******//
	localWoundProblem(dt,local_dt,local_parameters,c,prec,rho,CC,
			ip_phic_0,ip_mu_0,ip_kc_0,ip_a0c_0,ip_kappac_0,
			ip_phif_0,ip_kf_0,ip_a0f_0,ip_kappaf_0,
			ip_lamdaP_0,
			ip_phic,ip_mu,ip_kc,ip_a0c,ip_kappac,
			ip_phif,ip_kf,ip_a0f,ip_kappaf,
			ip_lamdaP,
			dThetadCC,dThetadrho,dThetadc);
	//*******//

	//
	// rename variables to make it easier to follow
	double phic_0 = ip_phic_0;
	double mu_0 = ip_mu_0;
	double kc_0 = ip_kc_0;
	Vector2d a0c_0 = ip_a0c_0;
	double kappac_0 = ip_kappac_0;
	//
	double phif_0 = ip_phif_0;
	double kf_0 = ip_kf_0;
	Vector2d a0f_0 = ip_a0f_0;
	double kappaf_0 = ip_kappaf_0;
	//
	Vector2d lamdaP_0 = ip_lamdaP_0;
	//
	double phic = ip_phic;
	double mu = ip_mu;
	double kc = ip_kc;
	Vector2d a0c = ip_a0c;
	double kappac = ip_kappac;
	//
	double phif = ip_phif;
	double kf = ip_kf;
	Vector2d a0f = ip_a0f;
	double kappaf = ip_kappaf;
	//
	Vector2d lamdaP = ip_lamdaP;
	//
	double lamdaP_a_0 = lamdaP_0(0);
	double lamdaP_s_0 = lamdaP_0(1);
	double lamdaP_a = lamdaP(0);
	double lamdaP_s = lamdaP(1);
	double thetaP = lamdaP_a*lamdaP_s;
	//
	// make sure the update preserved length
	double norma0c = sqrt(a0c.dot(a0c));
	if(fabs(norma0c-1.)>0.001){std::cout<<"update did not preserve unit length of a0c\n";}
	ip_a0c = a0c/(sqrt(a0c.dot(a0c)));
	a0c = a0c/(sqrt(a0c.dot(a0c)));
	double norma0f = sqrt(a0f.dot(a0f));
	if(fabs(norma0f-1.)>0.001){std::cout<<"update did not preserve unit length of a0f\n";}
	ip_a0f = a0f/(sqrt(a0f.dot(a0f)));
	a0f = a0f/(sqrt(a0f.dot(a0f)));
	//
	// unpack the derivatives wrt CC
	//
	Matrix2d dphicdCC; dphicdCC.setZero();
	dphicdCC(0,0) = dThetadCC(0);
	dphicdCC(0,1) = dThetadCC(1);
	dphicdCC(1,0) = dThetadCC(2);
	dphicdCC(1,1) = dThetadCC(3);
	Matrix2d dkcdCC; dkcdCC.setZero();
	dkcdCC(0,0) = dThetadCC(4);
	dkcdCC(0,1) = dThetadCC(5);
	dkcdCC(1,0) = dThetadCC(6);
	dkcdCC(1,1) = dThetadCC(7);
	Matrix2d da0cxdCC;da0cxdCC.setZero();
	da0cxdCC(0,0) = dThetadCC(8);
	da0cxdCC(0,1) = dThetadCC(9);
	da0cxdCC(1,0) = dThetadCC(10);
	da0cxdCC(1,1) = dThetadCC(11);
	Matrix2d da0cydCC;da0cydCC.setZero();
	da0cydCC(0,0) = dThetadCC(12);
	da0cydCC(0,1) = dThetadCC(13);
	da0cydCC(1,0) = dThetadCC(14);
	da0cydCC(1,1) = dThetadCC(15);
	Matrix2d dkappacdCC; dkappacdCC.setZero();
	dkappacdCC(0,0) = dThetadCC(16);
	dkappacdCC(0,1) = dThetadCC(17);
	dkappacdCC(1,0) = dThetadCC(18);
	dkappacdCC(1,1) = dThetadCC(19);
	//
	Matrix2d dphifdCC; dphifdCC.setZero();
	dphifdCC(0,0) = dThetadCC(20);
	dphifdCC(0,1) = dThetadCC(21);
	dphifdCC(1,0) = dThetadCC(22);
	dphifdCC(1,1) = dThetadCC(23);
	Matrix2d dkfdCC; dkfdCC.setZero();
	dkfdCC(0,0) = dThetadCC(24);
	dkfdCC(0,1) = dThetadCC(25);
	dkfdCC(1,0) = dThetadCC(26);
	dkfdCC(1,1) = dThetadCC(27);
	Matrix2d da0fxdCC;da0fxdCC.setZero();
	da0fxdCC(0,0) = dThetadCC(28);
	da0fxdCC(0,1) = dThetadCC(29);
	da0fxdCC(1,0) = dThetadCC(30);
	da0fxdCC(1,1) = dThetadCC(31);
	Matrix2d da0fydCC;da0fydCC.setZero();
	da0fydCC(0,0) = dThetadCC(32);
	da0fydCC(0,1) = dThetadCC(33);
	da0fydCC(1,0) = dThetadCC(34);
	da0fydCC(1,1) = dThetadCC(35);
	Matrix2d dkappafdCC; dkappafdCC.setZero();
	dkappafdCC(0,0) = dThetadCC(36);
	dkappafdCC(0,1) = dThetadCC(37);
	dkappafdCC(1,0) = dThetadCC(38);
	dkappafdCC(1,1) = dThetadCC(39);
	//
	Matrix2d dlamdaP_adCC; dlamdaP_adCC.setZero();
	dlamdaP_adCC(0,0) = dThetadCC(40);
	dlamdaP_adCC(0,1) = dThetadCC(41);
	dlamdaP_adCC(1,0) = dThetadCC(42);
	dlamdaP_adCC(1,1) = dThetadCC(43);
	Matrix2d dlamdaP_sdCC; dlamdaP_sdCC.setZero();
	dlamdaP_sdCC(0,0) = dThetadCC(44);
	dlamdaP_sdCC(0,1) = dThetadCC(45);
	dlamdaP_sdCC(1,0) = dThetadCC(46);
	dlamdaP_sdCC(1,1) = dThetadCC(47);
	//
	// unpack the derivatives wrt rho
	double dphicdrho = dThetadrho(0);
	double dkcdrho = dThetadrho(1);
	double da0cxdrho  = dThetadrho(2);
	double da0cydrho  = dThetadrho(3);
	double dkappacdrho  = dThetadrho(4);
	//
	double dphifdrho = dThetadrho(5);
	double dkfdrho = dThetadrho(6);
	double da0fxdrho  = dThetadrho(7);
	double da0fydrho  = dThetadrho(8);
	double dkappafdrho  = dThetadrho(9);
	//
	double dlamdaP_adrho  = dThetadrho(10);
	double dlamdaP_sdrho  = dThetadrho(11);
	//
	// unpack the derivatives wrt c
	double dphicdc = dThetadc(0);
	double dkcdc = dThetadc(1);
	double da0cxdc  = dThetadc(2);
	double da0cydc  = dThetadc(3);
	double dkappacdc  = dThetadc(4);
	//
	double dphifdc = dThetadc(5);
	double dkfdc = dThetadc(6);
	double da0fxdc  = dThetadc(7);
	double da0fydc  = dThetadc(8);
	double dkappafdc  = dThetadc(9);
	//
	double dlamdaP_adc  = dThetadc(10);
	double dlamdaP_sdc  = dThetadc(11);
	//
	//---------------------------------//



	//---------------------------------//
	// CALCULATE SOURCE AND FLUX
	//
	//---------------------------------//
	//
	Matrix2d SS_pas = evalSS_pas(FF,rho,Grad_rho,c,Grad_c,phic,mu,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,global_parameters);
	//
	//*************************************************************//
	// magnitude from systems bio
	// and derivatives as well
	Matrix2d SS_act;SS_act.setZero();
	Matrix3d DDact; DDact.setZero();
	Matrix2d dSSactdrho_explicit;dSSactdrho_explicit.setZero();
	Matrix2d dSSactdc_explicit;dSSactdc_explicit.setZero();
	// pass current deformation and all structures
	evalSS_act(FF,rho,Grad_rho,c,Grad_c,phic,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,global_parameters,load,
		SS_act,DDact,dSSactdrho_explicit,dSSactdc_explicit);
	//*************************************************************//

	Vector3d SS_voigt;
	SS_voigt(0) = SS_act(0,0)+SS_pas(0,0);
	SS_voigt(1) = SS_act(1,1)+SS_pas(1,1);
	SS_voigt(2) = SS_act(0,1)+SS_pas(0,1);

	//*************************************************************//
	// Flux and Source terms for the rho and the C
	Vector2d Q_rho,dQ_rhodrho_explicit,dQ_rhodc_explicit;
	Matrix2d dQ_rhoxdCC_explicit,dQ_rhoydCC_explicit,dQ_rhodGradrho,dQ_rhodGradc;
	Vector2d Q_c,dQ_cdrho_explicit,dQ_cdc_explicit;
	Matrix2d dQ_cxdCC_explicit,dQ_cydCC_explicit,dQ_cdGradrho,dQ_cdGradc;
	//
	evalQ_rho(FF,rho,Grad_rho,c,Grad_c,phic,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,global_parameters,
		Q_rho,dQ_rhoxdCC_explicit,dQ_rhoydCC_explicit,dQ_rhodrho_explicit,dQ_rhodGradrho,dQ_rhodc_explicit,dQ_rhodGradc);
	//
	evalQ_c(FF,rho,Grad_rho,c,Grad_c,phic,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,global_parameters,
		Q_c,dQ_cxdCC_explicit,dQ_cydCC_explicit,dQ_cdrho_explicit,dQ_cdGradrho,dQ_cdc_explicit,dQ_cdGradc);

	//
	double S_rho, dSrhodrho_explicit, dSrhodc_explicit;
	Matrix2d dSrhodCC_explicit;
	double S_c, dScdrho_explicit, dScdc_explicit;
	Matrix2d dScdCC_explicit;
	//
	evalS_rho(FF,rho,Grad_rho,c,Grad_c,phic,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,global_parameters,
		S_rho,dSrhodCC_explicit,dSrhodrho_explicit,dSrhodc_explicit);
	//
	evalS_c(FF,rho,Grad_rho,c,Grad_c,prec,phic,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,global_parameters,
		S_c,dScdCC_explicit,dScdrho_explicit,dScdc_explicit);
	//*************************************************************//

	//*************************************************************//
	//
	//---------------------------------//

	//---------------------------------//
	// ADD TO THE RESIDUAL
	//
	Matrix2d deltaFF,deltaCC;
	Vector3d deltaCC_voigt;
	for(int nodei=0;nodei<3;nodei++){
		for(int coordi=0;coordi<2;coordi++){
			// alternatively, define the deltaCC
			deltaFF = Ebasis[coordi]*Grad_R[nodei].transpose();
			deltaCC = deltaFF.transpose()*FF + FF.transpose()*deltaFF;
			deltaCC_voigt = Vector3d(deltaCC(0,0),deltaCC(1,1),2.*deltaCC(1,0));
			Re_x(nodei*2+coordi) += 0.5*Jac*SS_voigt.dot(deltaCC_voigt)*thick_0/2.;
		}
	}
	//
	Re_rho(0) += Jac*(((rho-rho_0)/dt - S_rho)*thetaP*R[0] - Grad_R[0].dot(Q_rho))*thick_0/2;
	Re_rho(1) += Jac*(((rho-rho_0)/dt - S_rho)*thetaP*R[1] - Grad_R[1].dot(Q_rho))*thick_0/2;
	Re_rho(2) += Jac*(((rho-rho_0)/dt - S_rho)*thetaP*R[2] - Grad_R[2].dot(Q_rho))*thick_0/2;
	//
	Re_c(0) += Jac*(((c-c_0)/dt - S_c)*thetaP*R[0] - Grad_R[0].dot(Q_c))*thick_0/2;
	Re_c(1) += Jac*(((c-c_0)/dt - S_c)*thetaP*R[1] - Grad_R[1].dot(Q_c))*thick_0/2;
	Re_c(2) += Jac*(((c-c_0)/dt - S_c)*thetaP*R[2] - Grad_R[2].dot(Q_c))*thick_0/2;
	//
	//---------------------------------//



	//---------------------------------//
	// TANGENTS
	//---------------------------------//



	//---------------------------------//
	// NUMERICAL DERIVATIVES
	//
	// NOTE:
	// the chain rule is insane, might as well use numerical tangent, partially.
	// proposed: numerical tangent wrt structural parameters
	// then use the derivatives dThetadCC, dThetadrho, dThetadC
	// derivative wrt to CC is done analytically
	//
	double epsilon = 1e-7;
	//
	// structural parameters
	double phic_plus = phic + epsilon;
	double phic_minus= phic - epsilon;
	double kc_plus = kc + epsilon;
	double kc_minus= kc - epsilon;
	Vector2d a0c_plus_x= a0c + epsilon*Ebasis[0];
	Vector2d a0c_minus_x = a0c - epsilon*Ebasis[0];
	Vector2d a0c_plus_y= a0c + epsilon*Ebasis[1];
	Vector2d a0c_minus_y= a0c - epsilon*Ebasis[1];
	double kappac_plus = kappac + epsilon;
	double kappac_minus =kappac - epsilon;
	//
	double phif_plus = phif + epsilon;
	double phif_minus= phif - epsilon;
	double kf_plus = kf + epsilon;
	double kf_minus= kf - epsilon;
	Vector2d a0f_plus_x= a0f + epsilon*Ebasis[0];
	Vector2d a0f_minus_x = a0f - epsilon*Ebasis[0];
	Vector2d a0f_plus_y= a0f + epsilon*Ebasis[1];
	Vector2d a0f_minus_y= a0f - epsilon*Ebasis[1];
	double kappaf_plus = kappaf + epsilon;
	double kappaf_minus =kappaf - epsilon;
	//
	Vector2d lamdaP_plus_a = lamdaP + epsilon*Ebasis[0];
	Vector2d lamdaP_minus_a = lamdaP - epsilon*Ebasis[0];
	Vector2d lamdaP_plus_s = lamdaP + epsilon*Ebasis[1];
	Vector2d lamdaP_minus_s = lamdaP - epsilon*Ebasis[1];
	//
	// fluxes and sources
	Matrix2d SS_plus,SS_minus;
	Vector2d Q_rho_plus,Q_rho_minus;
	Vector2d Q_c_plus,Q_c_minus;
	double S_rho_plus,S_rho_minus;
	double S_c_plus,S_c_minus;
	//
	// phic
	evalFluxesSources(global_parameters,phic_plus,mu,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,FF,rho,c,Grad_rho,Grad_c,prec, SS_plus,Q_rho_plus,S_rho_plus,Q_c_plus,S_c_plus);
	evalFluxesSources(global_parameters,phic_minus,mu,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,FF,rho,c,Grad_rho,Grad_c,prec, SS_minus,Q_rho_minus,S_rho_minus,Q_c_minus,S_c_minus);
	Matrix2d dSSdphic = (1./(2.*epsilon))*(SS_plus-SS_minus);
	Vector2d dQ_rhodphic = (1./(2.*epsilon))*(Q_rho_plus-Q_rho_minus);
	Vector2d dQ_cdphic = (1./(2.*epsilon))*(Q_c_plus-Q_c_minus);
	double dS_rhodphic = (1./(2.*epsilon))*(S_rho_plus-S_rho_minus);
	double dS_cdphic = (1./(2.*epsilon))*(S_c_plus-S_c_minus);
	//
	// kc
	evalFluxesSources(global_parameters,phic,mu,kc_plus,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,FF,rho,c,Grad_rho,Grad_c,prec, SS_plus,Q_rho_plus,S_rho_plus,Q_c_plus,S_c_plus);
	evalFluxesSources(global_parameters,phic,mu,kc_minus,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,FF,rho,c,Grad_rho,Grad_c,prec, SS_minus,Q_rho_minus,S_rho_minus,Q_c_minus,S_c_minus);
	Matrix2d dSSdkc = (1./(2.*epsilon))*(SS_plus-SS_minus);
	Vector2d dQ_rhodkc = (1./(2.*epsilon))*(Q_rho_plus-Q_rho_minus);
	Vector2d dQ_cdkc = (1./(2.*epsilon))*(Q_c_plus-Q_c_minus);
	double dS_rhodkc = (1./(2.*epsilon))*(S_rho_plus-S_rho_minus);
	double dS_cdkc = (1./(2.*epsilon))*(S_c_plus-S_c_minus);
	//
	// a0cx
	evalFluxesSources(global_parameters,phic,mu,kc,a0c_plus_x,kappac,phif,kf,a0f,kappaf,lamdaP,FF,rho,c,Grad_rho,Grad_c,prec, SS_plus,Q_rho_plus,S_rho_plus,Q_c_plus,S_c_plus);
	evalFluxesSources(global_parameters,phic,mu,kc,a0c_minus_x,kappac,phif,kf,a0f,kappaf,lamdaP,FF,rho,c,Grad_rho,Grad_c,prec, SS_minus,Q_rho_minus,S_rho_minus,Q_c_minus,S_c_minus);
	Matrix2d dSSda0cx = (1./(2.*epsilon))*(SS_plus-SS_minus);
	Vector2d dQ_rhoda0cx = (1./(2.*epsilon))*(Q_rho_plus-Q_rho_minus);
	Vector2d dQ_cda0cx = (1./(2.*epsilon))*(Q_c_plus-Q_c_minus);
	double dS_rhoda0cx = (1./(2.*epsilon))*(S_rho_plus-S_rho_minus);
	double dS_cda0cx = (1./(2.*epsilon))*(S_c_plus-S_c_minus);
	//
	// a0cy
	evalFluxesSources(global_parameters,phic,mu,kc,a0c_plus_y,kappac,phif,kf,a0f,kappaf,lamdaP,FF,rho,c,Grad_rho,Grad_c,prec, SS_plus,Q_rho_plus,S_rho_plus,Q_c_plus,S_c_plus);
	evalFluxesSources(global_parameters,phic,mu,kc,a0c_minus_y,kappac,phif,kf,a0f,kappaf,lamdaP,FF,rho,c,Grad_rho,Grad_c,prec, SS_minus,Q_rho_minus,S_rho_minus,Q_c_minus,S_c_minus);
	Matrix2d dSSda0cy = (1./(2.*epsilon))*(SS_plus-SS_minus);
	Vector2d dQ_rhoda0cy = (1./(2.*epsilon))*(Q_rho_plus-Q_rho_minus);
	Vector2d dQ_cda0cy = (1./(2.*epsilon))*(Q_c_plus-Q_c_minus);
	double dS_rhoda0cy = (1./(2.*epsilon))*(S_rho_plus-S_rho_minus);
	double dS_cda0cy = (1./(2.*epsilon))*(S_c_plus-S_c_minus);
	//
	// kappac
	evalFluxesSources(global_parameters,phic,mu,kc,a0c,kappac_plus,phif,kf,a0f,kappaf,lamdaP,FF,rho,c,Grad_rho,Grad_c,prec, SS_plus,Q_rho_plus,S_rho_plus,Q_c_plus,S_c_plus);
	evalFluxesSources(global_parameters,phic,mu,kc,a0c,kappac_minus,phif,kf,a0f,kappaf,lamdaP,FF,rho,c,Grad_rho,Grad_c,prec, SS_minus,Q_rho_minus,S_rho_minus,Q_c_minus,S_c_minus);
	Matrix2d dSSdkappac = (1./(2.*epsilon))*(SS_plus-SS_minus);
	Vector2d dQ_rhodkappac = (1./(2.*epsilon))*(Q_rho_plus-Q_rho_minus);
	Vector2d dQ_cdkappac = (1./(2.*epsilon))*(Q_c_plus-Q_c_minus);
	double dS_rhodkappac = (1./(2.*epsilon))*(S_rho_plus-S_rho_minus);
	double dS_cdkappac = (1./(2.*epsilon))*(S_c_plus-S_c_minus);
	//
	// phif
	evalFluxesSources(global_parameters,phic,mu,kc,a0c,kappac,phif_plus,kf,a0f,kappaf,lamdaP,FF,rho,c,Grad_rho,Grad_c,prec, SS_plus,Q_rho_plus,S_rho_plus,Q_c_plus,S_c_plus);
	evalFluxesSources(global_parameters,phic,mu,kc,a0c,kappac,phif_minus,kf,a0f,kappaf,lamdaP,FF,rho,c,Grad_rho,Grad_c,prec, SS_minus,Q_rho_minus,S_rho_minus,Q_c_minus,S_c_minus);
	Matrix2d dSSdphif = (1./(2.*epsilon))*(SS_plus-SS_minus);
	Vector2d dQ_rhodphif = (1./(2.*epsilon))*(Q_rho_plus-Q_rho_minus);
	Vector2d dQ_cdphif = (1./(2.*epsilon))*(Q_c_plus-Q_c_minus);
	double dS_rhodphif = (1./(2.*epsilon))*(S_rho_plus-S_rho_minus);
	double dS_cdphif = (1./(2.*epsilon))*(S_c_plus-S_c_minus);
	//
	// kf
	evalFluxesSources(global_parameters,phic,mu,kc,a0c,kappac,phif,kf_plus,a0f,kappaf,lamdaP,FF,rho,c,Grad_rho,Grad_c,prec, SS_plus,Q_rho_plus,S_rho_plus,Q_c_plus,S_c_plus);
	evalFluxesSources(global_parameters,phic,mu,kc,a0c,kappac,phif,kf_minus,a0f,kappaf,lamdaP,FF,rho,c,Grad_rho,Grad_c,prec, SS_minus,Q_rho_minus,S_rho_minus,Q_c_minus,S_c_minus);
	Matrix2d dSSdkf = (1./(2.*epsilon))*(SS_plus-SS_minus);
	Vector2d dQ_rhodkf = (1./(2.*epsilon))*(Q_rho_plus-Q_rho_minus);
	Vector2d dQ_cdkf = (1./(2.*epsilon))*(Q_c_plus-Q_c_minus);
	double dS_rhodkf = (1./(2.*epsilon))*(S_rho_plus-S_rho_minus);
	double dS_cdkf = (1./(2.*epsilon))*(S_c_plus-S_c_minus);
	//
	// a0fx
	evalFluxesSources(global_parameters,phic,mu,kc,a0c,kappac,phif,kf,a0f_plus_x,kappaf,lamdaP,FF,rho,c,Grad_rho,Grad_c,prec, SS_plus,Q_rho_plus,S_rho_plus,Q_c_plus,S_c_plus);
	evalFluxesSources(global_parameters,phic,mu,kc,a0c,kappac,phif,kf,a0f_minus_x,kappaf,lamdaP,FF,rho,c,Grad_rho,Grad_c,prec, SS_minus,Q_rho_minus,S_rho_minus,Q_c_minus,S_c_minus);
	Matrix2d dSSda0fx = (1./(2.*epsilon))*(SS_plus-SS_minus);
	Vector2d dQ_rhoda0fx = (1./(2.*epsilon))*(Q_rho_plus-Q_rho_minus);
	Vector2d dQ_cda0fx = (1./(2.*epsilon))*(Q_c_plus-Q_c_minus);
	double dS_rhoda0fx = (1./(2.*epsilon))*(S_rho_plus-S_rho_minus);
	double dS_cda0fx = (1./(2.*epsilon))*(S_c_plus-S_c_minus);
	//
	// a0fy
	evalFluxesSources(global_parameters,phic,mu,kc,a0c,kappac,phif,kf,a0f_plus_y,kappaf,lamdaP,FF,rho,c,Grad_rho,Grad_c,prec, SS_plus,Q_rho_plus,S_rho_plus,Q_c_plus,S_c_plus);
	evalFluxesSources(global_parameters,phic,mu,kc,a0c,kappac,phif,kf,a0f_minus_y,kappaf,lamdaP,FF,rho,c,Grad_rho,Grad_c,prec, SS_minus,Q_rho_minus,S_rho_minus,Q_c_minus,S_c_minus);
	Matrix2d dSSda0fy = (1./(2.*epsilon))*(SS_plus-SS_minus);
	Vector2d dQ_rhoda0fy = (1./(2.*epsilon))*(Q_rho_plus-Q_rho_minus);
	Vector2d dQ_cda0fy = (1./(2.*epsilon))*(Q_c_plus-Q_c_minus);
	double dS_rhoda0fy = (1./(2.*epsilon))*(S_rho_plus-S_rho_minus);
	double dS_cda0fy = (1./(2.*epsilon))*(S_c_plus-S_c_minus);
	//
	// kappaf
	evalFluxesSources(global_parameters,phic,mu,kc,a0c,kappac,phif,kf,a0f,kappaf_plus,lamdaP,FF,rho,c,Grad_rho,Grad_c,prec, SS_plus,Q_rho_plus,S_rho_plus,Q_c_plus,S_c_plus);
	evalFluxesSources(global_parameters,phic,mu,kc,a0c,kappac,phif,kf,a0f,kappaf_minus,lamdaP,FF,rho,c,Grad_rho,Grad_c,prec, SS_minus,Q_rho_minus,S_rho_minus,Q_c_minus,S_c_minus);
	Matrix2d dSSdkappaf = (1./(2.*epsilon))*(SS_plus-SS_minus);
	Vector2d dQ_rhodkappaf = (1./(2.*epsilon))*(Q_rho_plus-Q_rho_minus);
	Vector2d dQ_cdkappaf = (1./(2.*epsilon))*(Q_c_plus-Q_c_minus);
	double dS_rhodkappaf = (1./(2.*epsilon))*(S_rho_plus-S_rho_minus);
	double dS_cdkappaf = (1./(2.*epsilon))*(S_c_plus-S_c_minus);
	//
	// lamdaP_a
	evalFluxesSources(global_parameters,phic,mu,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP_plus_a,FF,rho,c,Grad_rho,Grad_c,prec, SS_plus,Q_rho_plus,S_rho_plus,Q_c_plus,S_c_plus);
	evalFluxesSources(global_parameters,phic,mu,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP_minus_a,FF,rho,c,Grad_rho,Grad_c,prec, SS_minus,Q_rho_minus,S_rho_minus,Q_c_minus,S_c_minus);
	Matrix2d dSSdlamdaPa = (1./(2.*epsilon))*(SS_plus-SS_minus);
	Vector2d dQ_rhodlamdaPa = (1./(2.*epsilon))*(Q_rho_plus-Q_rho_minus);
	Vector2d dQ_cdlamdaPa = (1./(2.*epsilon))*(Q_c_plus-Q_c_minus);
	double dS_rhodlamdaPa = (1./(2.*epsilon))*(S_rho_plus-S_rho_minus);
	double dS_cdlamdaPa = (1./(2.*epsilon))*(S_c_plus-S_c_minus);
	//
	// lamdaP_s
	evalFluxesSources(global_parameters,phic,mu,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP_plus_s,FF,rho,c,Grad_rho,Grad_c,prec, SS_plus,Q_rho_plus,S_rho_plus,Q_c_plus,S_c_plus);
	evalFluxesSources(global_parameters,phic,mu,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP_minus_s,FF,rho,c,Grad_rho,Grad_c,prec, SS_minus,Q_rho_minus,S_rho_minus,Q_c_minus,S_c_minus);
	Matrix2d dSSdlamdaPs = (1./(2.*epsilon))*(SS_plus-SS_minus);
	Vector2d dQ_rhodlamdaPs = (1./(2.*epsilon))*(Q_rho_plus-Q_rho_minus);
	Vector2d dQ_cdlamdaPs = (1./(2.*epsilon))*(Q_c_plus-Q_c_minus);
	double dS_rhodlamdaPs = (1./(2.*epsilon))*(S_rho_plus-S_rho_minus);
	double dS_cdlamdaPs = (1./(2.*epsilon))*(S_c_plus-S_c_minus);
	//
	//---------------------------------//


	//---------------------------------//
	// MECHANICS TANGENT
	//
	//--------------------------------------------------//
	// build DD, the voigt version of CCCC = dSS_dCC
	// voigt table
	Vector3d voigt_table_I_i(0,1,0);
	Vector3d voigt_table_I_j(0,1,1);
	Vector3d voigt_table_J_k(0,1,0);
	Vector3d voigt_table_J_l(0,1,1);
	//
	Matrix3d DDpas,DDstruct,DDtot;
	int ii,jj,kk,ll;
	for(int II=0;II<3;II++){
		for(int JJ=0;JJ<3;JJ++){
			ii = voigt_table_I_i(II);
			jj = voigt_table_I_j(II);
			kk = voigt_table_J_k(JJ);
			ll = voigt_table_J_l(JJ);

			// structural
			DDstruct(II,JJ) = dSSdphic(ii,jj)*dphicdCC(kk,ll) + dSSdkc(ii,jj)*dkcdCC(kk,ll) + dSSda0cx(ii,jj)*da0cxdCC(kk,ll) + dSSda0cy(ii,jj)*da0cydCC(kk,ll) + dSSdkappac(ii,jj)*dkappacdCC(kk,ll)
							+ dSSdphif(ii,jj)*dphifdCC(kk,ll) + dSSdkf(ii,jj)*dkfdCC(kk,ll) + dSSda0fx(ii,jj)*da0fxdCC(kk,ll) + dSSda0fy(ii,jj)*da0fydCC(kk,ll) + dSSdkappaf(ii,jj)*dkappafdCC(kk,ll)
							+ dSSdlamdaPa(ii,jj)*dlamdaP_adCC(kk,ll) + dSSdlamdaPs(ii,jj)*dlamdaP_sdCC(kk,ll);
		}
	}

	DDpas = eval_DDpas(FF,rho,Grad_rho,c,Grad_c,phic,mu,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,global_parameters);
	DDtot = DDact + DDstruct + DDpas;
	//--------------------------------------------------//

	// Derivatives for rho and c
	//
	Matrix2d dSSdrho = dSSactdrho_explicit +
					   dSSdphic*dphicdrho + dSSdkc*dkcdrho + dSSda0cx*da0cxdrho + dSSda0cy*da0cydrho+dSSdkappac*dkappacdrho+
					   dSSdphif*dphifdrho + dSSdkf*dkfdrho + dSSda0fx*da0fxdrho + dSSda0fy*da0fydrho+dSSdkappaf*dkappafdrho+
					   dSSdlamdaPa*dlamdaP_adrho + dSSdlamdaPs*dlamdaP_sdrho;
	Vector3d dSSdrho_voigt(dSSdrho(0,0),dSSdrho(1,1),dSSdrho(0,1));

	Matrix2d dSSdc =   dSSactdc_explicit +
					   dSSdphic*dphicdc + dSSdkc*dkcdc + dSSda0cx*da0cxdc + dSSda0cy*da0cydc+dSSdkappac*dkappacdc+
					   dSSdphif*dphifdc + dSSdkf*dkfdc + dSSda0fx*da0fxdc + dSSda0fy*da0fydc+dSSdkappaf*dkappafdc+
					   dSSdlamdaPa*dlamdaP_adc + dSSdlamdaPs*dlamdaP_sdc;
	Vector3d dSSdc_voigt(dSSdc(0,0),dSSdc(1,1),dSSdc(0,1));


	// some other declared variables
	Matrix2d linFF,linCC,lindeltaCC;
	Vector3d linCC_voigt,lindeltaCC_voigt;
	//
	// Loop over nodes and coordinates twice and assemble the corresponding entry
	for(int nodei=0;nodei<3;nodei++){
		for(int coordi=0;coordi<2;coordi++){
			deltaFF = Ebasis[coordi]*Grad_R[nodei].transpose();
			deltaCC = deltaFF.transpose()*FF + FF.transpose()*deltaFF;
			deltaCC_voigt = Vector3d(deltaCC(0,0),deltaCC(1,1),2.*deltaCC(1,0));
			for(int nodej=0;nodej<3;nodej++){
				for(int coordj=0;coordj<2;coordj++){

					//-----------//
					// Ke_X_X
					//-----------//

					// material part of the tangent
					linFF =  Ebasis[coordj]*Grad_R[nodej].transpose();
					linCC = linFF.transpose()*FF + FF.transpose()*linFF;
					linCC_voigt = Vector3d(linCC(0,0),linCC(1,1),2.*linCC(1,0));
					//
					Ke_x_x(nodei*2+coordi,nodej*2+coordj) += Jac*deltaCC_voigt.dot(DDtot*linCC_voigt)*thick_0/2;
					//
					// geometric part of the tangent
					lindeltaCC = deltaFF.transpose()*linFF + linFF.transpose()*deltaFF;
					lindeltaCC_voigt = Vector3d(lindeltaCC(0,0),lindeltaCC(1,1),2.*lindeltaCC(0,1));
					//
					Ke_x_x(nodei*2+coordi,nodej*2+coordj) += Jac*SS_voigt.dot(lindeltaCC_voigt)*thick_0/2;

				}

				//-----------//
				// Ke_x_rho
				//-----------//

				Ke_x_rho(nodei*2+coordi,nodej) += Jac*dSSdrho_voigt.dot(deltaCC_voigt)*R[nodej]*thick_0/2;

				//-----------//
				// Ke_x_c
				//-----------//

				Ke_x_c(nodei*2+coordi,nodej) += Jac*dSSdc_voigt.dot(deltaCC_voigt)*R[nodej]*thick_0/2;

			}
		}
	}

	//-----------------//
	// RHO and C
	//-----------------//

	// total derivatives
	double dS_rhodrho = dSrhodrho_explicit +
						dS_rhodphic*dphicdrho + dS_rhodkc*dkcdrho + dS_rhoda0cx*da0cxdrho + dS_rhoda0cy*da0cydrho+dS_rhodkappac*dkappacdrho+
						dS_rhodphif*dphifdrho + dS_rhodkf*dkfdrho + dS_rhoda0fx*da0fxdrho + dS_rhoda0fy*da0fydrho+dS_rhodkappaf*dkappafdrho+
						dS_rhodlamdaPa*dlamdaP_adrho+dS_rhodlamdaPs*dlamdaP_sdrho;
	double dS_rhodc =   dSrhodc_explicit +
						dS_rhodphic*dphicdc + dS_rhodkc*dkcdc + dS_rhoda0cx*da0cxdc + dS_rhoda0cy*da0cydc+dS_rhodkappac*dkappacdc+
						dS_rhodphif*dphifdc + dS_rhodkf*dkfdc + dS_rhoda0fx*da0fxdc + dS_rhoda0fy*da0fydc+dS_rhodkappaf*dkappafdc+
						dS_rhodlamdaPa*dlamdaP_adc+dS_rhodlamdaPs*dlamdaP_sdc;
	double dS_cdrho =   dScdrho_explicit +
						dS_cdphic*dphicdrho + dS_cdkc*dkcdrho + dS_cda0cx*da0cxdrho + dS_cda0cy*da0cydrho+dS_cdkappac*dkappacdrho+
						dS_cdphif*dphifdrho + dS_cdkf*dkfdrho + dS_cda0fx*da0fxdrho + dS_cda0fy*da0fydrho+dS_cdkappaf*dkappafdrho+
						dS_cdlamdaPa*dlamdaP_adrho+dS_cdlamdaPs*dlamdaP_sdrho;
	double dS_cdc =     dScdc_explicit +
						dS_cdphic*dphicdc + dS_cdkc*dkcdc + dS_cda0cx*da0cxdc + dS_cda0cy*da0cydc+dS_cdkappac*dkappacdc+
						dS_cdphif*dphifdc + dS_cdkf*dkfdc + dS_cda0fx*da0fxdc + dS_cda0fy*da0fydc+dS_cdkappaf*dkappafdc+
						dS_cdlamdaPa*dlamdaP_adc+dS_cdlamdaPs*dlamdaP_sdc;
	// wrt Mechanics
	Matrix2d dS_rhodCC =dSrhodCC_explicit +
						dS_rhodphic*dphicdCC + dS_rhodkc*dkcdCC + dS_rhoda0cx*da0cxdCC + dS_rhoda0cy*da0cydCC+dS_rhodkappac*dkappacdCC+
						dS_rhodphif*dphifdCC + dS_rhodkf*dkfdCC + dS_rhoda0fx*da0fxdCC + dS_rhoda0fy*da0fydCC+dS_rhodkappaf*dkappafdCC+
						dS_rhodlamdaPa*dlamdaP_adCC+dS_rhodlamdaPs*dlamdaP_sdCC;
	Vector3d dS_rhodCC_voigt(dS_rhodCC(0,0),dS_rhodCC(1,1),dS_rhodCC(0,1));
	Matrix2d dS_cdCC =  dScdCC_explicit +
						dS_cdphic*dphicdCC + dS_cdkc*dkcdCC + dS_cda0cx*da0cxdCC + dS_cda0cy*da0cydCC+dS_cdkappac*dkappacdCC+
						dS_cdphif*dphifdCC + dS_cdkf*dkfdCC + dS_cda0fx*da0fxdCC + dS_cda0fy*da0fydCC+dS_cdkappaf*dkappafdCC+
						dS_cdlamdaPa*dlamdaP_adCC+dS_cdlamdaPs*dlamdaP_sdCC;
	Vector3d dS_cdCC_voigt(dS_cdCC(0,0),dS_cdCC(1,1),dS_cdCC(0,1));

	// fluxes. Chain rule for these
	Vector2d dQ_rhodrho = dQ_rhodrho_explicit \
						+ dQ_rhodphic*dphicdrho + dQ_rhodkc*dkcdrho + dQ_rhoda0cx*da0cxdrho + dQ_rhoda0cy*da0cydrho + dQ_rhodkappac*dkappacdrho
						+ dQ_rhodphif*dphifdrho + dQ_rhodkf*dkfdrho + dQ_rhoda0fx*da0fxdrho + dQ_rhoda0fy*da0fydrho + dQ_rhodkappaf*dkappafdrho
						+ dQ_rhodlamdaPa*dlamdaP_adrho + dQ_rhodlamdaPs*dlamdaP_sdrho;
	Vector2d dQ_rhodc =  dQ_rhodc_explicit \
						+ dQ_rhodphic*dphicdc + dQ_rhodkc*dkcdc + dQ_rhoda0cx*da0cxdc + dQ_rhoda0cy*da0cydc + dQ_rhodkappac*dkappacdc
						+ dQ_rhodphif*dphifdc + dQ_rhodkf*dkfdc + dQ_rhoda0fx*da0fxdc + dQ_rhoda0fy*da0fydc + dQ_rhodkappaf*dkappafdc
						+ dQ_rhodlamdaPa*dlamdaP_adc + dQ_rhodlamdaPs*dlamdaP_sdc;
	Vector2d dQ_cdrho = dQ_cdrho_explicit \
						+ dQ_cdphic*dphicdrho + dQ_cdkc*dkcdrho + dQ_cda0cx*da0cxdrho + dQ_cda0cy*da0cydrho + dQ_cdkappac*dkappacdrho
						+ dQ_cdphif*dphifdrho + dQ_cdkf*dkfdrho + dQ_cda0fx*da0fxdrho + dQ_cda0fy*da0fydrho + dQ_cdkappaf*dkappafdrho
						+ dQ_cdlamdaPa*dlamdaP_adrho + dQ_cdlamdaPs*dlamdaP_sdrho;
	Vector2d dQ_cdc =  dQ_cdc_explicit \
						+ dQ_cdphic*dphicdc + dQ_cdkc*dkcdc + dQ_cda0cx*da0cxdc + dQ_cda0cy*da0cydc + dQ_cdkappac*dkappacdc
						+ dQ_cdphif*dphifdc + dQ_cdkf*dkfdc + dQ_cda0fx*da0fxdc + dQ_cda0fy*da0fydc + dQ_cdkappaf*dkappafdc
						+ dQ_cdlamdaPa*dlamdaP_adc + dQ_cdlamdaPs*dlamdaP_sdc;
	// the dQdGradrho is only the explicit because the structural parameters don't depend on the gradient of cells
	// or the gradient of chemical, not here at least.
	Matrix2d dQ_rhoxdCC = dQ_rhoxdCC_explicit \
						+ dQ_rhodphic(0)*dphicdCC + dQ_rhodkc(0)*dkcdCC + dQ_rhoda0cx(0)*da0cxdCC + dQ_rhoda0cy(0)*da0cydCC + dQ_rhodkappac(0)*dkappacdCC
						+ dQ_rhodphif(0)*dphifdCC + dQ_rhodkf(0)*dkfdCC + dQ_rhoda0fx(0)*da0fxdCC + dQ_rhoda0fy(0)*da0fydCC + dQ_rhodkappaf(0)*dkappafdCC
						+ dQ_rhodlamdaPa(0)*dlamdaP_adCC + dQ_rhodlamdaPs(0)*dlamdaP_sdCC;
	Matrix2d dQ_rhoydCC = dQ_rhoydCC_explicit \
						+ dQ_rhodphic(1)*dphicdCC + dQ_rhodkc(1)*dkcdCC + dQ_rhoda0cx(1)*da0cxdCC + dQ_rhoda0cy(1)*da0cydCC + dQ_rhodkappac(1)*dkappacdCC
						+ dQ_rhodphif(1)*dphifdCC + dQ_rhodkf(1)*dkfdCC + dQ_rhoda0fx(1)*da0fxdCC + dQ_rhoda0fy(1)*da0fydCC + dQ_rhodkappaf(1)*dkappafdCC
						+ dQ_rhodlamdaPa(1)*dlamdaP_adCC + dQ_rhodlamdaPs(1)*dlamdaP_sdCC;
	Vector3d dQ_rhoxdCC_voigt=Vector3d(dQ_rhoxdCC(0,0),dQ_rhoxdCC(1,1),dQ_rhoxdCC(0,1));
	Vector3d dQ_rhoydCC_voigt=Vector3d(dQ_rhoydCC(0,0),dQ_rhoydCC(1,1),dQ_rhoydCC(0,1));

	Matrix2d dQ_cxdCC = dQ_cxdCC_explicit \
						+ dQ_cdphic(0)*dphicdCC + dQ_cdkc(0)*dkcdCC + dQ_cda0cx(0)*da0cxdCC + dQ_cda0cy(0)*da0cydCC + dQ_cdkappac(0)*dkappacdCC
						+ dQ_cdphif(0)*dphifdCC + dQ_cdkf(0)*dkfdCC + dQ_cda0fx(0)*da0fxdCC + dQ_cda0fy(0)*da0fydCC + dQ_cdkappaf(0)*dkappafdCC
						+ dQ_cdlamdaPa(0)*dlamdaP_adCC + dQ_cdlamdaPs(0)*dlamdaP_sdCC;
	Matrix2d dQ_cydCC = dQ_cydCC_explicit \
						+ dQ_cdphic(1)*dphicdCC + dQ_cdkc(1)*dkcdCC + dQ_cda0cx(1)*da0cxdCC + dQ_cda0cy(1)*da0cydCC + dQ_cdkappac(1)*dkappacdCC
						+ dQ_cdphif(1)*dphifdCC + dQ_cdkf(1)*dkfdCC + dQ_cda0fx(1)*da0fxdCC + dQ_cda0fy(1)*da0fydCC + dQ_cdkappaf(1)*dkappafdCC
						+ dQ_cdlamdaPa(1)*dlamdaP_adCC + dQ_cdlamdaPs(1)*dlamdaP_sdCC;
	Vector3d dQ_cxdCC_voigt = Vector3d(dQ_cxdCC(0,0),dQ_cxdCC(1,1),dQ_cxdCC(0,1));
	Vector3d dQ_cydCC_voigt = Vector3d(dQ_cydCC(0,0),dQ_cydCC(1,1),dQ_cydCC(0,1));
	// assemble into matrix
	double  dthetaPdrho = lamdaP_a*dlamdaP_sdrho + lamdaP_s*dlamdaP_adrho;
	double  dthetaPdc = lamdaP_a*dlamdaP_sdc + lamdaP_s*dlamdaP_adc;

	Vector2d linQ_rho;
	Vector2d linQ_c;
	double linQ_rhox,linQ_rhoy;
	double linQ_cx,linQ_cy;
	//
	Matrix2d dthetaPdCC = lamdaP_s*dlamdaP_adCC+ lamdaP_a*dlamdaP_sdCC;
	Vector3d dthetaPdCC_voigt;
	dthetaPdCC_voigt(0) = dthetaPdCC(0,0);
	dthetaPdCC_voigt(1) = dthetaPdCC(1,1);
	dthetaPdCC_voigt(2) = dthetaPdCC(0,1);
	//
	for(int nodei=0;nodei<3;nodei++){
		for(int nodej=0;nodej<3;nodej++){
			for(int coordj=0;coordj<2;coordj++){

				linFF =  Ebasis[coordj]*Grad_R[nodej].transpose();
				linCC = linFF.transpose()*FF + FF.transpose()*linFF;
				linCC_voigt = Vector3d(linCC(0,0),linCC(1,1),2.*linCC(0,1));
				linQ_rhox =dQ_rhoxdCC_voigt.dot( linCC_voigt);
				linQ_rhoy =dQ_rhoydCC_voigt.dot( linCC_voigt);
				linQ_rho(0) = linQ_rhox;
				linQ_rho(1) = linQ_rhoy;
				linQ_cx =dQ_cxdCC_voigt.dot( linCC_voigt);
				linQ_cy =dQ_cydCC_voigt.dot( linCC_voigt);
				linQ_c(0) = linQ_cx;
				linQ_c(1) = linQ_cy;
				//-----------//
				// Ke_rho_X
				//-----------//

				Ke_rho_x(nodei,nodej*2+coordj)+= -thetaP*R[nodei]*dS_rhodCC_voigt.dot(linCC_voigt)*Jac*thick_0/2  + Jac*(((rho-rho_0)/dt - S_rho)*dthetaPdCC_voigt.dot(linCC_voigt)*R[nodei])*thick_0/2 - Jac*thick_0/2*Grad_R[nodei].dot(linQ_rho)  ;

				//-----------//
				// Ke_c_X
				//-----------//

				Ke_c_x(nodei,nodej*2+coordj)+= -thetaP*R[nodei]*dS_cdCC_voigt.dot(linCC_voigt)*Jac*thick_0/2 + Jac*thick_0/2*(((c-c_0)/dt - S_c)*dthetaPdCC_voigt.dot(linCC_voigt)*R[nodei]) - Jac*thick_0/2*Grad_R[nodei].dot(linQ_c)  ;

			}

			//-----------//
			// Ke_rho_rho
			//-----------//

			Ke_rho_rho(nodei,nodej) += Jac*thick_0/2*(thetaP*R[nodei]*R[nodej]/dt -1.*thetaP*R[nodei]*dS_rhodrho*R[nodej]   -1.* Grad_R[nodei].dot(dQ_rhodGradrho*Grad_R[nodej] + dQ_rhodrho*R[nodej]))
				+ Jac*thick_0/2*(((rho-rho_0)/dt - S_rho)*dthetaPdrho*R[nodei]*R[nodej]);

			//-----------//
			// Ke_rho_c
			//-----------//

			Ke_rho_c(nodei,nodej) += Jac*thick_0/2*(-thetaP*R[nodei]*dS_rhodc*R[nodej] -1.* Grad_R[nodei].dot(dQ_rhodGradc*Grad_R[nodej]+ dQ_rhodc*R[nodej]))
				+ Jac*thick_0/2*(((rho-rho_0)/dt - S_rho)*dthetaPdc*R[nodei]*R[nodej]);

			//-----------//
			// Ke_c_rho
			//-----------//

			Ke_c_rho(nodei,nodej) += Jac*thick_0/2*(-thetaP*R[nodei]*dS_cdrho*R[nodej] -1.* Grad_R[nodei].dot(dQ_cdGradrho*Grad_R[nodej]+ dQ_cdrho*R[nodej]))
				+ Jac*thick_0/2*(((c-c_0)/dt - S_c)*dthetaPdrho*R[nodei]*R[nodej]);

			//-----------//
			// Ke_c_c
			//-----------//

			Ke_c_c(nodei,nodej) += Jac*thick_0/2*(thetaP*R[nodei]*R[nodej]/dt -thetaP* R[nodei]*dS_cdc*R[nodej] -1.* Grad_R[nodei].dot(dQ_cdGradc*Grad_R[nodej]+dQ_cdc*R[nodej]))
				+ Jac*thick_0/2*(((c-c_0)/dt - S_c)*dthetaPdc*R[nodei]*R[nodej]);
		}
	}
}

// ELEMENT RESIDUAL AND TANGENT for load stepping
void evalMechanics(
	double load_step,double load,
	const Matrix2d &ip_Jac,
	const std::vector<double> &global_parameters,const std::vector<double> &local_parameters,
	const std::vector<double> &node_rho_0, const std::vector<double> &node_c_0,
	double ip_phic_0,double ip_mu_0,double ip_kc_0,const Vector2d ip_a0c_0,double ip_kappac_0,
	double ip_phif_0,double ip_kf_0,const Vector2d &ip_a0f_0,double ip_kappaf_0,
	const Vector2d &ip_lamdaP_0,
	const std::vector<Vector2d> &node_x,
	VectorXd &Re_x,MatrixXd &Ke_x_x,
	Matrix2d &ip_FF, Matrix2d &ip_sig_all)
{

	//---------------------------------//
	// INPUT
	//  dt: time step
	//  local_dt: local time step for remodeling
	//	elem_jac_IP: jacobians at the integration points, needed for the deformation grad
	//  global_param: material parameters for global newton
	//  local_param: material parameters for local newton
	//  Xi_t: global fields at previous time step
	//  Theta_t: structural fields at previous time steps
	//  Xi: current guess of the global fields
	//  Theta: current guess of the structural fields
	//	node_x: deformed positions
	//
	// OUTPUT
	//  Re: all residuals
	//  Ke: all tangents
	//
	// Algorithm
	//  0. Loop over integration points
	//	1. F,rho,c,nabla_rho,nabla_c: deformation at IP
	//  2. LOCAL NEWTON -> update the current guess of the structural parameters
	//  3. Fe,Fp
	//	4. Se_pas,Se_act,S
	//	5. Qrho,Srho,Qc,Sc
	//  6. Residuals
	//  7. Tangents
	//---------------------------------//



	//---------------------------------//
	// GLOBAL PARAMETERS
	//
	// double k0 = global_parameters[0]; // neo hookean
	double k2c = global_parameters[1]; // nonlinear exponential collagen
	double k2f = global_parameters[2]; // nonlinear exponential fibronectin
	double t_rho = global_parameters[3]; // force of fibroblasts
	double t_rho_c = global_parameters[4]; // force of myofibroblasts enhanced by chemical
	double K_t_c = global_parameters[5]; // saturation of chemical on force
	double D_rhorho = global_parameters[6]; // diffusion of cells
	double D_rhoc = global_parameters[7]; // diffusion of chemotactic gradient
	double Fc_max = global_parameters[8]; // effect of chemical on the diffusion constant
	double D_cc = global_parameters[9]; // diffusion of chemical
	double p_rho =global_parameters[10]; // production of fibroblasts naturally
	double p_rho_c = global_parameters[11]; // production enhanced by the chem
	double p_rho_theta = global_parameters[12]; // mechanosensing
	double K_rho_c= global_parameters[13]; // saturation of cell production by chemical
	double K_rho_rho = global_parameters[14]; // saturation of cell by cell
	double d_rho = global_parameters[15] ;// decay of cells
	double theta_phy = global_parameters[16]; // physiological state of area stretch
	double gamma_c_thetaE = global_parameters[17]; // sensitivity of heviside function
	double p_c_rho = global_parameters[18];// production of C by cells
	double p_c_thetaE = global_parameters[19]; // coupling of elastic and chemical
	double K_c_c = global_parameters[20];// saturation of chem by chem
	double d_c = global_parameters[21]; // decay of chemical
	double thick_0  =  global_parameters[22]; // reference thickness
	//
	//
	//---------------------------------//



	//---------------------------------//
	// GLOBAL VARIABLES
	// Initialize the residuals to zero and declare some global stuff
	Re_x.setZero();
	Ke_x_x.setZero();
	int n_nodes = 3;
	std::vector<Vector2d> Ebasis; Ebasis.clear();
	Matrix2d Rot90;Rot90<<0.,-1.,1.,0.;
	Ebasis.push_back(Vector2d(1.,0.)); Ebasis.push_back(Vector2d(0.,1.));
	//---------------------------------//
	//--------------------------------------//
	// LOOP OVER INTEGRATION POINTS, only 1!
	//--------------------------------------//

	// array with integration points
	// shape function and derivatives
    double xi  = 1./3.;
    double eta = 1./3.;
    std::vector<double> R   ={1.-xi-eta,xi,eta};
    std::vector<double> Rxi ={-1.,1.,0.};
    std::vector<double> Reta={-1.,0.,1.};
	double Jac = 1./ip_Jac.determinant();

	// declare variables and gradients at IP
	std::vector<Vector2d> dRdXi;dRdXi.clear();
	Vector2d dxdxi,dxdeta;
	dxdxi.setZero();dxdeta.setZero();
	double rho=0.; Vector2d drhodXi; drhodXi.setZero();
	double c=0.; Vector2d dcdXi; dcdXi.setZero();
	//
	for(int ni=0;ni<n_nodes;ni++)
	{
		dRdXi.push_back(Vector2d(Rxi[ni],Reta[ni]));

		dxdxi += node_x[ni]*Rxi[ni];
		dxdeta += node_x[ni]*Reta[ni];

		rho += node_rho_0[ni]*R[ni];
		drhodXi(0) += node_rho_0[ni]*Rxi[ni];
		drhodXi(1) += node_rho_0[ni]*Reta[ni];

		c += node_c_0[ni]*R[ni];
		dcdXi(0) += node_c_0[ni]*Rxi[ni];
		dcdXi(1) += node_c_0[ni]*Reta[ni];
	}
	//
	//---------------------------------//



	//---------------------------------//
	// EVAL GRADIENTS
	//
	// Deformation gradient and strain
	// assemble the columns
	Matrix2d dxdXi; dxdXi<<dxdxi(0),dxdeta(0),dxdxi(1),dxdeta(1);
	// F = dxdX
	Matrix2d FF = dxdXi*ip_Jac.transpose();
	// STORE deformation gradient tensor in the output
	ip_FF = FF;

	// Gradient of concentrations in current configuration
	Matrix2d dXidx     = dxdXi.inverse();
	Vector2d grad_rho  = dXidx.transpose()*drhodXi;
	Vector2d grad_c    = dXidx.transpose()*dcdXi;
	//
	// Gradient of concentrations in reference
	Vector2d Grad_rho = ip_Jac*drhodXi;
	Vector2d Grad_c   = ip_Jac*dcdXi;
	//
	// Gradient of basis functions for the nodes in reference
	std::vector<Vector2d> Grad_R;Grad_R.clear();
	Grad_R.push_back(ip_Jac*dRdXi[0]);
	Grad_R.push_back(ip_Jac*dRdXi[1]);
	Grad_R.push_back(ip_Jac*dRdXi[2]);
	//
	// Gradient of basis functions in deformed configuration
	std::vector<Vector2d> grad_R;grad_R.clear();
	grad_R.push_back(dXidx.transpose()*dRdXi[0]);
	grad_R.push_back(dXidx.transpose()*dRdXi[1]);
	grad_R.push_back(dXidx.transpose()*dRdXi[2]);

	// rename variables to make it easier
	double phic = ip_phic_0;
	double mu = ip_mu_0;
	double kc = ip_kc_0;
	Vector2d a0c = ip_a0c_0;
	double kappac = ip_kappac_0;
	//
	double phif = ip_phif_0;
	double kf = ip_kf_0;
	Vector2d a0f = ip_a0f_0;
	double kappaf = ip_kappaf_0;
	//
	Vector2d lamdaP = ip_lamdaP_0;
	//
	double lamdaP_a = lamdaP(0);
	double lamdaP_s = lamdaP(1);
	double thetaP = lamdaP_a*lamdaP_s;
	//

	//---------------------------------//
	// CALCULATE SOURCE AND FLUX
	//
	Matrix2d SS_pas = evalSS_pas(FF,rho,Grad_rho,c,Grad_c,phic,mu,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,global_parameters);

	// STORE cauchy stress tensor in output vector
	Matrix2d sigma = FF*SS_pas*FF.transpose();
	ip_sig_all = sigma;

	//*************************************************************//
	// magnitude from systems bio
	// and derivatives as well
	Matrix2d SS_act;SS_act.setZero();
	Matrix3d DDact; DDact.setZero();
	Matrix2d dSSactdrho_explicit;dSSactdrho_explicit.setZero();
	Matrix2d dSSactdc_explicit;dSSactdc_explicit.setZero();
	// pass current deformation and all structures
	evalSS_act(FF,rho,Grad_rho,c,Grad_c,phic,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,global_parameters,load,
		SS_act,DDact,dSSactdrho_explicit,dSSactdc_explicit);
	//*************************************************************//

	Vector3d SS_voigt;
	SS_voigt(0) = SS_act(0,0)+SS_pas(0,0);
	SS_voigt(1) = SS_act(1,1)+SS_pas(1,1);
	SS_voigt(2) = SS_act(0,1)+SS_pas(0,1);

	//---------------------------------//
	// ADD TO THE RESIDUAL
	//
	Matrix2d deltaFF,deltaCC;
	Vector3d deltaCC_voigt;
	for(int nodei=0;nodei<3;nodei++){
		for(int coordi=0;coordi<2;coordi++){
			// alternatively, define the deltaCC
			deltaFF = Ebasis[coordi]*Grad_R[nodei].transpose();
			deltaCC = deltaFF.transpose()*FF + FF.transpose()*deltaFF;
			deltaCC_voigt = Vector3d(deltaCC(0,0),deltaCC(1,1),2.*deltaCC(1,0));
			Re_x(nodei*2+coordi) += 0.5*Jac*SS_voigt.dot(deltaCC_voigt)*thick_0/2.;
		}
	}
	//
	//---------------------------------//
	// TANGENTS
	//---------------------------------//

	//---------------------------------//
	// MECHANICS TANGENT
	Matrix3d DDpas, DDtot;
	DDpas = eval_DDpas(FF,rho,Grad_rho,c,Grad_c,phic,mu,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,global_parameters);
	DDtot = DDact + DDpas;

	//--------------------------------------------------//

	// some other declared variables
	Matrix2d linFF,linCC,lindeltaCC;
	Vector3d linCC_voigt,lindeltaCC_voigt;
	//
	// Loop over nodes and coordinates twice and assemble the corresponding entry
	for(int nodei=0;nodei<3;nodei++){
		for(int coordi=0;coordi<2;coordi++){
			deltaFF = Ebasis[coordi]*Grad_R[nodei].transpose();
			deltaCC = deltaFF.transpose()*FF + FF.transpose()*deltaFF;
			deltaCC_voigt = Vector3d(deltaCC(0,0),deltaCC(1,1),2.*deltaCC(1,0));
			for(int nodej=0;nodej<3;nodej++){
				for(int coordj=0;coordj<2;coordj++){

					//-----------//
					// Ke_X_X
					//-----------//

					// material part of the tangent
					linFF =  Ebasis[coordj]*Grad_R[nodej].transpose();
					linCC = linFF.transpose()*FF + FF.transpose()*linFF;
					linCC_voigt = Vector3d(linCC(0,0),linCC(1,1),2.*linCC(1,0));
					//
					Ke_x_x(nodei*2+coordi,nodej*2+coordj) += Jac*deltaCC_voigt.dot(DDtot*linCC_voigt)*thick_0/2;
					//
					// geometric part of the tangent
					lindeltaCC = deltaFF.transpose()*linFF + linFF.transpose()*deltaFF;
					lindeltaCC_voigt = Vector3d(lindeltaCC(0,0),lindeltaCC(1,1),2.*lindeltaCC(0,1));
					//
					Ke_x_x(nodei*2+coordi,nodej*2+coordj) += Jac*SS_voigt.dot(lindeltaCC_voigt)*thick_0/2;

				}
			}
		}
	}

}

//========================================================//
// EVAL SOURCE AND FLUX
//========================================================//

// Sources and Fluxes are :stress, biological fluxes and sources
void evalFluxesSources(
	const std::vector<double> &global_parameters,
	double phic,double mu, double kc, const Vector2d &a0c,double kappac,
	double phif,double kf, const Vector2d &a0f,double kappaf,
	const Vector2d &lamdaP,
	const Matrix2d &FF,double rho, double c, const Vector2d &Grad_rho, const Vector2d &Grad_c,double prec,
	Matrix2d &SS, Vector2d &Q_rho,double &S_rho, Vector2d &Q_c,double &S_c)
{

	// double k0 = global_parameters[0]; // neo hookean
	double k2c = global_parameters[1]; // nonlinear exponential collagen
	double k2f = global_parameters[2]; // nonlinear exponential fibronectin
	double t_rho = global_parameters[3]; // force of fibroblasts
	double t_rho_c = global_parameters[4]; // force of myofibroblasts enhanced by chemical
	double K_t_c = global_parameters[5]; // saturation of chemical on force
	double D_rhorho = global_parameters[6]; // diffusion of cells
	double D_rhoc = global_parameters[7]; // diffusion of chemotactic gradient
	double Fc_max = global_parameters[8]; // effect of chemical on diffusion weight
	double D_cc = global_parameters[9]; // diffusion of chemical
	double p_rho =global_parameters[10]; // production of fibroblasts naturally
	double p_rho_c = global_parameters[11]; // production enhanced by the chem
	double p_rho_theta = global_parameters[12]; // mechanosensing
	double K_rho_c= global_parameters[13]; // saturation of cell production by chemical
	double K_rho_rho = global_parameters[14]; // saturation of cell by cell
	double d_rho = global_parameters[15] ;// decay of cells
	double theta_phy = global_parameters[16]; // physiological state of area stretch
	double gamma_c_thetaE = global_parameters[17]; // sensitivity of heviside function
	double p_c_rho = global_parameters[18];// production of C by cells
	double p_c_thetaE = global_parameters[19]; // coupling of elastic and chemical
	double K_c_c = global_parameters[20];// saturation of chem by chem
	double d_c = global_parameters[21]; // decay of chemical
	double thick_0 = global_parameters[22]; // thickness in reference
	double D_precprec = global_parameters[23]; // diffusion of neutrophil/pdgf
	double d_prec = global_parameters[24]; // decay of neutrophil/pdgf
	double p_c_prec = global_parameters[25]; // production of TGFb1/Macrophage by neutrophil/PDGF
	double prec_healthy =  global_parameters[26]; // healthy level of precursor 
	double c_target =  global_parameters[27]; // target level of TGFb1 to ensure that healthy value = 1
	double rho_healthy =  global_parameters[28]; // healthy level of rho 

	// pull back to the reference
	Matrix2d SS_pas = evalSS_pas(FF,rho,Grad_rho,c,Grad_c,phic,mu,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,global_parameters);
	//*************************************************************//
	// magnitude from systems bio
	double load = 1;
	Matrix2d SS_act = evalSS_act(FF,rho,Grad_rho,c,Grad_c,phic,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,global_parameters,load);
	//*************************************************************//

	// total stress
	SS = SS_pas + SS_act;

	//*************************************************************//
	// Flux and Source terms for the rho and the C
	Q_rho = evalQ_rho(FF,rho,Grad_rho,c,Grad_c,phic,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,global_parameters);
	Q_c =   evalQ_c(FF,rho,Grad_rho,c,Grad_c,phic,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,global_parameters);
	S_rho = evalS_rho(FF,rho,Grad_rho,c,Grad_c,phic,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,global_parameters);
	S_c =   evalS_c(FF,rho,Grad_rho,c,Grad_c,prec,phic,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,global_parameters);
	//*************************************************************//

}




//========================================================//
// LOCAL PROBLEM: structural update
//========================================================//

void localWoundProblem(
	double global_dt, double local_dt_frac,
	const std::vector<double> &local_parameters,
	double c,double prec, double rho,const Matrix2d &CC,
	double phic_0,double mu_0,double kc_0, const Vector2d &a0c_0, double kappac_0,
	double phif_0,double kf_0, const Vector2d &a0f_0, double kappaf_0,
	const Vector2d &lamdaP_0,
	double &phic,double &mu,double &kc, Vector2d &a0c, double &kappac,
	double &phif,double &kf, Vector2d &a0f, double &kappaf,
	Vector2d &lamdaP,
	VectorXd &dThetadCC, VectorXd &dThetadrho, VectorXd &dThetadc)
{


	//---------------------------------//
	//
	// INPUT
	// 	local_param: material parameters
	//	rho: value of the cell density at the point
	//	c: concentration at the point
	//	CC: deformation at the point (right cauchy green)
	//	Theta_t: value of the parameters at previous time step
	//
	// OUTPUT
	//	Theta: value of the parameters at the current time step
	//	dThetadCC: derivative of Theta wrt global mechanics (CC)
	// 	dThetadrho: derivative of Theta wrt global rho
	// 	dThetadc: derivative of Theta wrt global C
	//
	//---------------------------------//


	//---------------------------------//
	// Parameters
	//---------------------------------//
	//
	// fiber alignment
	double tau_omega_c = local_parameters[0]; // time constant for angular reorientation of collagen
	double tau_omega_f = local_parameters[1]; // time constant for angular reorientation of fibronectin
	//
	// dispersion parameter
	double tau_kappa_c   = local_parameters[2]; // time constant
	double gamma_kappa_c = local_parameters[3]; // exponent of the principal stretch ratio
	double tau_kappa_f   = local_parameters[4]; // time constant
	double gamma_kappa_f = local_parameters[5]; // exponent of the principal stretch ratio
	//
	// permanent contracture/growth
	double tau_lamdaP_a = local_parameters[6]; // time constant for direction a
	double tau_lamdaP_s = local_parameters[7]; // time constant for direction s
	//
	double thick_0  =  local_parameters[8]; // reference thickness
	double phic_vol =  local_parameters[9]; // volume of collagen associated to healthy collagen fraction.
	double phif_vol =  local_parameters[10]; // volume ot fibronectin associated to healthy state.
	//
	double vartheta_e =  local_parameters[18]; // physiological stretch
	// REMARKS: some of the local parameters are going to be overwritten in the biology functions
	// also, the volume fractions above are fold-wise, that means phif_healthy = 0 is not allowed
	// that should not be the case any way.
	//
	//---------------------------------//

	//---------------------------------//
	// Declare variables and pre compute
	//---------------------------------//
	//
	//Matrix2d CCinv = CC.inverse();
	double theta = sqrt(CC.determinant());
	double PIE = 3.14159;
	//
	// variables to be updated
	double phic_dot_plus, phic_dot_minus, phif_dot_plus, phif_dot_minus;
	double phic_dot,phif_dot;
	double kc_dot_plus,kc_dot_minus,kf_dot_plus,kf_dot_minus;
	double kc_dot, kf_dot;
	//
	Vector2d a0c_old=a0c_0, a0f_old=a0f_0;
	Vector2d lamdaP_old=lamdaP_0;
	double kappac_old=kappac_0, kappaf_old=kappaf_0;
	//
	// tangent variables
	Matrix2d dphicdotplusdCC,dphicdotminusdCC;
	double dphicdotplusdrho,dphicdotplusdc,dphicdotminusdrho, dphicdotminusdc;
	Matrix2d dphifdotplusdCC,dphifdotminusdCC;
	double dphifdotplusdrho,dphifdotplusdc,dphifdotminusdrho, dphifdotminusdc;
	Matrix2d dkcdotplusdCC,dkcdotminusdCC;
	double dkcdotplusdrho,dkcdotplusdc,dkcdotminusdrho, dkcdotminusdc;
	Matrix2d dkfdotplusdCC,dkfdotminusdCC;
	double dkfdotplusdrho,dkfdotplusdc,dkfdotminusdrho, dkfdotminusdc;
	//
	Matrix2d dphicdotdCC, dphifdotdCC ;
	Matrix2d dphicdCC; dphicdCC.setZero();
	Matrix2d dphifdCC; dphifdCC.setZero();
	Matrix2d dkcdotdCC , dkfdotdCC;
	Matrix2d dkcdCC; dkcdCC.setZero();
	Matrix2d dkfdCC; dkfdCC.setZero();
	//
	Matrix2d dCe_aa_cdCC, dCe_ss_cdCC, dCe_as_cdCC;
	Matrix2d dCe_aa_fdCC, dCe_ss_fdCC, dCe_as_fdCC;
	//
	double aux00c, aux00f,aux01c, aux01f ;
	double dlamda1dCe_aa_c,dlamda1dCe_ss_c,dlamda1dCe_as_c,dlamda0dCe_aa_c,dlamda0dCe_ss_c,dlamda0dCe_as_c;
	Matrix2d dlamda1dCC, dlamda0dCC;
	double dsinVarthetacdlamda1,dsinVarthetacdCe_aa_c,dsinVarthetacdCe_as_c,dsinVarthetacdCe_ss_c;
	double dsinVarthetafdlamda1,dsinVarthetafdCe_aa_f,dsinVarthetafdCe_as_f,dsinVarthetafdCe_ss_f;
	Matrix2d dsinVarthetacdCC,dsinVarthetafdCC;
	Matrix2d domegacdCC,domegafdCC;
	Matrix2d dRomegacdomegac,dRomegafdomegaf;
	Vector2d da0c,da0f;
	Matrix2d aux_da0cxdCC,aux_da0cydCC, aux_da0fxdCC, aux_da0fydCC;
	//
	Matrix2d da0cxdCC; da0cxdCC.setZero();
	Matrix2d da0cydCC; da0cydCC.setZero();
	Matrix2d da0fxdCC; da0fxdCC.setZero();
	Matrix2d da0fydCC; da0fydCC.setZero();
	//
	Matrix2d dkappacdCC;dkappacdCC.setZero();
	Matrix2d dkappafdCC;dkappafdCC.setZero();
	//
	Matrix2d dlamdaE_adCC,dlamdaE_sdCC;
	//
	Matrix2d dlamdaPadCC; dlamdaPadCC.setZero();
	Matrix2d dlamdaPsdCC; dlamdaPsdCC.setZero();
	//
	double dphicdotdrho, dphifdotdrho;
	double dphicdrho=0.0; double dphifdrho=0.0;
	double dkcdotdrho, dkfdotdrho;
	double dkcdrho=0.0; double dkfdrho=0.0;
	double domegacdrho,domegafdrho;
	Vector2d da0cdrho,da0fdrho;da0cdrho.setZero();da0fdrho.setZero();
	double dkappacdrho=0.0, dkappafdrho = 0.0;
	double dlamdaPadrho = 0.0, dlamdaPsdrho = 0.0;
	//
	double dphicdotdc, dphifdotdc;
	double dphicdc=0.0; double dphifdc=0.0;
	double dkcdotdc, dkfdotdc;
	double dkcdc=0.0; double dkfdc=0.0;
	double domegacdc,domegafdc;
	Vector2d da0cdc,da0fdc;da0cdc.setZero();da0fdc.setZero();
	double dkappacdc=0.0, dkappafdc = 0.0;
	double dlamdaPadc = 0.0, dlamdaPsdc = 0.0;
	//
	Vector2d s0c; s0c.setZero();
	Vector2d s0f; s0f.setZero();
	double Ce_aa_c,Ce_as_c,Ce_ss_c,Ce_sa_c;
	double Ce_aa_f,Ce_as_f,Ce_ss_f,Ce_sa_f;
	double lamda0, lamda1;
	double sinVartheta_c,sinVartheta_f,omega_c,omega_f;
	Matrix2d Romega_c,Romega_f;
	double kappacdot,kappafdot;
	double lamdaE_a,lamdaE_s;
	double lamdaN, thick;
	double thetaP, thetaE;
	Vector2d lamdaP_dot;lamdaP_dot.setZero();
	//---------------------------------//


	//---------------------------------//
	// LOOP OVER LOCAL TIME
	//---------------------------------//
	// The local problem has a different time step
	// The local time step is percent of the global time step
	double local_time=0;
	//double local_time_steps= 1.0/local_dt_frac;
	int local_time_steps = 1;
	double dt = global_dt*local_dt_frac;
	dt = global_dt;
	// initialize the current with the previous
	phic = phic_0; kc = kc_0; a0c = a0c_0; kappac = kappac_0;
	phif = phif_0; kf = kf_0; a0f = a0f_0; kappaf = kappaf_0;
	lamdaP(0) = lamdaP_0(0); lamdaP(1) = lamdaP_0(1);
	//
	//---------------------------------//

	// LOOP OVER LOCAL TIME

	for(int stepi=0;stepi<local_time_steps;stepi++){

		// Explicit Euler

		//********************************************//
		// Biology
		//
		eval_phic_dot_plus(CC,rho,c,phic,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,local_parameters,
			phic_dot_plus,  dphicdotplusdCC, dphicdotplusdrho, dphicdotplusdc);
		eval_phic_dot_minus(CC,rho,c,phic,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,local_parameters,
			phic_dot_minus, dphicdotminusdCC, dphicdotminusdrho, dphicdotminusdc);
		//
		eval_phif_dot_plus(CC,rho,c,phic,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,local_parameters,
			phif_dot_plus,  dphifdotplusdCC,  dphifdotplusdrho, dphifdotplusdc);
		eval_phif_dot_minus(CC,rho,c,prec, phic,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,local_parameters,
			phif_dot_minus, dphifdotminusdCC, dphifdotminusdrho, dphifdotminusdc);
		//
		eval_kc_dot_plus(CC,rho,c,phic,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,local_parameters,
			kc_dot_plus,  dkcdotplusdCC,  dkcdotplusdrho, dkcdotplusdc);
		eval_kc_dot_minus(CC,rho,c,phic,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,local_parameters,
			kc_dot_minus, dkcdotminusdCC, dkcdotminusdrho, dkcdotminusdc);
		//
		eval_kf_dot_plus(CC,rho,c,phic,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,local_parameters,
			kf_dot_plus,  dkfdotplusdCC, dkfdotplusdrho, dkfdotplusdc);
		eval_kf_dot_minus(CC,rho,c,prec,phic,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,local_parameters,
			kf_dot_minus, dkfdotminusdCC, dkfdotminusdrho, dkfdotminusdc);
		//********************************************//


		//********************************************//
		// Update variables (euler steps)
		//
		// Update collagen and fibronectin content
		phic_dot = (phic_dot_plus - phic_dot_minus);
		phif_dot = (phif_dot_plus - phif_dot_minus);
		phic = phic + dt*(phic_dot_plus - phic_dot_minus);
		phif = phif + dt*(phif_dot_plus - phif_dot_minus);
		kc = kc + dt*(kc_dot_plus-kc_dot_minus);
		kf = kf + dt*(kf_dot_plus-kf_dot_minus);
		//
		// Update the fiber direction for collagen and fibronectin
		//
		// given the current guess of the direction make the choice of s
		s0c(0)=-a0c(1); s0c(1) = a0c(0);
		s0f(0)=-a0f(1); s0f(1) = a0f(0);
		// compute the principal eigenvalues and eigenvectors of Ce as a function of
		// the structural variables, namely lamdaP and a0. Note we can express CCe in the principal directions of both phic and phif
		// note how Ce depends on values at the previous time step because they
		// have not been updated yet
		Ce_aa_c = 0; Ce_as_c = 0; Ce_ss_c = 0, Ce_sa_c = 0;
		Ce_aa_f = 0; Ce_as_f = 0; Ce_ss_f = 0, Ce_sa_f = 0;
		for(int coordi=0;coordi<2;coordi++){
			for(int coordj=0;coordj<2;coordj++){
				Ce_aa_c += CC(coordi,coordj)*a0c(coordi)*a0c(coordj)/(lamdaP(0)*lamdaP(0));
				Ce_ss_c += CC(coordi,coordj)*s0c(coordi)*s0c(coordj)/(lamdaP(1)*lamdaP(1));
				Ce_as_c += CC(coordi,coordj)*a0c(coordi)*s0c(coordj)/(lamdaP(0)*lamdaP(1));
				Ce_sa_c += CC(coordi,coordj)*a0c(coordj)*s0c(coordi)/(lamdaP(0)*lamdaP(1));
				//
				Ce_aa_f += CC(coordi,coordj)*a0f(coordi)*a0f(coordj)/(lamdaP(0)*lamdaP(0));
				Ce_ss_f += CC(coordi,coordj)*s0f(coordi)*s0f(coordj)/(lamdaP(1)*lamdaP(1));
				Ce_as_f += CC(coordi,coordj)*a0f(coordi)*s0f(coordj)/(lamdaP(0)*lamdaP(1));
				Ce_sa_f += CC(coordi,coordj)*a0f(coordj)*s0f(coordi)/(lamdaP(0)*lamdaP(1));
			}
		}
		//
		// Principal stretches are independent of the basis, so either CCe_c or CCe_f can be used
		lamda1 = ((Ce_aa_c + Ce_ss_c) + sqrt( (Ce_aa_c-Ce_ss_c)*(Ce_aa_c-Ce_ss_c) + 4.0*Ce_as_c*Ce_as_c))/2.; // the eigenvalue is a squared number by notation
		lamda0 = ((Ce_aa_c + Ce_ss_c) - sqrt( (Ce_aa_c-Ce_ss_c)*(Ce_aa_c-Ce_ss_c) + 4.0*Ce_as_c*Ce_as_c))/2.; // the eigenvalue is a squared number by notation
		if(fabs(lamda1-lamda0)<1e-8){
			// equal eigenvalues means multiple of identity -> you can't possibly reorient.
			sinVartheta_c = 0.;
		}else if(fabs(lamda1-Ce_aa_c)<1e-8){
			// eigenvector in the direction of a0c already -> no need to reorient since you are already there
			sinVartheta_c = 0.;
		}else{
			// if eigenvalues are not the same and the principal eigenvalue is not already in the direction of a0c
			sinVartheta_c = (lamda1-Ce_aa_c)/sqrt(Ce_as_c*Ce_as_c + (lamda1-Ce_aa_c)*(lamda1-Ce_aa_c));
		}
		if(fabs(lamda1-lamda0)<1e-8){
			// equal eigenvalues means multiple of identity -> you can't possibly reorient.
			sinVartheta_f = 0.;
		}else if(fabs(lamda1-Ce_aa_f)<1e-8){
			// eigenvector in the direction of a0f already -> no need to reorient since you are already there
			sinVartheta_f = 0.;
		}else{
			// if eigenvalues are not the same and the principal eigenvalue is not already in the direction of a0c
			sinVartheta_f = (lamda1-Ce_aa_f)/sqrt(Ce_as_f*Ce_as_f + (lamda1-Ce_aa_f)*(lamda1-Ce_aa_f));
		}
		// angular velocity of reorientation of collagen
		omega_c = 0.0;//*((2.*PIE*phic_dot_plus)/(tau_omega_c))*lamda1*sinVartheta_c; // lamda1 is already squared by notation
		// compute the rotation tensor
		Romega_c(0,0) = cos(omega_c*dt); Romega_c(0,1) = -sin(omega_c*dt);
		Romega_c(1,0) = sin(omega_c*dt); Romega_c(1,1) = cos(omega_c*dt);
		// amgular velocity of reorientation of fibronectin
		omega_f = 0.0;//*((2.*PIE*(phic_dot_plus+phif_dot_plus))/(tau_omega_f))*lamda1*sinVartheta_f; // lamda1 is already squared by notation
		// compute the rotation tensor
		Romega_f(0,0) = cos(omega_f*dt); Romega_f(0,1) = -sin(omega_f*dt);
		Romega_f(1,0) = sin(omega_f*dt); Romega_f(1,1) = cos(omega_f*dt);
		// rotate the previous fiber
		a0c_old = a0c; a0f_old = a0f;
		a0c = Romega_c*a0c;
		a0f = Romega_f*a0f;

		// Update the dispersion
		kappacdot = 0.0;//*(phic_dot_plus/tau_kappa_c)*( pow(lamda0/lamda1,gamma_kappa_c)/2.  -kappac);
		kappafdot = 0.0;//*((phic_dot_plus+phif_dot_plus)/tau_kappa_f)*( pow(lamda0/lamda1,gamma_kappa_f)/2.  -kappaf);
		kappac_old = kappac; kappaf_old = kappaf;
		kappac = kappac + kappacdot*dt;
		kappaf = kappaf + kappafdot*dt;

		// Update permanent deformation
		// elastic stretches of the directions a and s
		lamdaE_a = sqrt(Ce_aa_c);
		lamdaE_s = sqrt(Ce_ss_c);
		// elastic fixing plus addition of material in isotropic growth
		thetaP = lamdaP(0)*lamdaP(1); // old growth variable
		thetaE = theta/thetaP;
		lamdaN = 1.0/thetaE;
		thick = thick_0*lamdaN;

		// growth is a sum of the fixation of the elastic deformation plus the volume added due to the collagen deposition
		// the material deposited contributes to isotropic growth in area. Would there be the need to add growth along fiber directions?
		// lamdaP_dot(0) = phic_dot_plus*(lamdaE_a-1.1)/tau_lamdaP_a + (phic_dot*phic_vol)/thick;
		// lamdaP_dot(1) = phic_dot_plus*(lamdaE_s-1.1)/tau_lamdaP_s + (phic_dot*phic_vol)/thick;
		// lamdaP_dot(0) = phic_dot_plus*(lamdaE_a-1.)/tau_lamdaP_a + (phic_dot*phic_vol)/thick;
		// lamdaP_dot(1) = phic_dot_plus*(lamdaE_s-1.)/tau_lamdaP_s + (phic_dot*phic_vol)/thick;
		//
		if ( lamdaE_a >= sqrt(vartheta_e) )
			lamdaP_dot(0) = phic_dot_plus*(lamdaE_a-sqrt(vartheta_e))/tau_lamdaP_a;
		else if ( lamdaE_a <= 1.0 )
			lamdaP_dot(0) = phic_dot_plus*(lamdaE_a-1.0)/tau_lamdaP_a;
		else
			lamdaP_dot(0) = 0.0;
		// 
		if ( lamdaE_s >= sqrt(vartheta_e) )
			lamdaP_dot(1) = phic_dot_plus*(lamdaE_s-sqrt(vartheta_e))/tau_lamdaP_s;
		else if ( lamdaE_s <= 1.0 )
			lamdaP_dot(1) = phic_dot_plus*(lamdaE_s-1.0)/tau_lamdaP_s;
		else
			lamdaP_dot(1) = 0.0;

		lamdaP_old = lamdaP;
		lamdaP = lamdaP + dt*lamdaP_dot;
		//
		//********************************************//

		//********************************************//
		// Tangents
		//********************************************//

		//----------//
		// CC
		//----------//

		// phic and phif
		dphicdotdCC = (dphicdotplusdCC - dphicdotminusdCC);
		dphifdotdCC = (dphifdotplusdCC - dphifdotminusdCC);
		dphicdCC = dphicdCC + dt*dphicdotdCC;
		dphifdCC = dphifdCC + dt*dphifdotdCC;
		//
		// kc and kf
		dkcdotdCC = (dkcdotplusdCC - dkcdotminusdCC);
		dkfdotdCC = (dkfdotplusdCC - dkfdotminusdCC);
		dkcdCC = dkcdCC + dt*dkcdotdCC;
		dkfdCC = dkfdCC + dt*dkfdotdCC;
		//
		// a0c and a0f
		// explicit derivatives of Ce wrt CC
		//
		dCe_aa_cdCC.setZero(); dCe_ss_cdCC.setZero(); dCe_as_cdCC.setZero();
		dCe_aa_fdCC.setZero(); dCe_ss_fdCC.setZero(); dCe_as_fdCC.setZero();
		//
		for(int coordi=0;coordi<2;coordi++){
			for(int coordj=0;coordj<2;coordj++){
				dCe_aa_cdCC(coordi,coordj) += a0c_old(coordi)*a0c_old(coordj)/(lamdaP_old(0)*lamdaP_old(0));
				dCe_ss_cdCC(coordi,coordj) += s0c(coordi)*s0c(coordj)/(lamdaP_old(1)*lamdaP_old(1));
				dCe_as_cdCC(coordi,coordj) += a0c_old(coordi)*s0c(coordj)/(lamdaP_old(0)*lamdaP_old(1));
				//
				dCe_aa_fdCC(coordi,coordj) += a0f_old(coordi)*a0f_old(coordj)/(lamdaP_old(0)*lamdaP_old(0));
				dCe_ss_fdCC(coordi,coordj) += s0f(coordi)*s0f(coordj)/(lamdaP_old(1)*lamdaP_old(1));
				dCe_as_fdCC(coordi,coordj) += a0f_old(coordi)*s0f(coordj)/(lamdaP_old(0)*lamdaP_old(1));
			}
		}
		aux00c = sqrt( (Ce_aa_c-Ce_ss_c)*(Ce_aa_c-Ce_ss_c) + 4*Ce_as_c*Ce_as_c);
		if(aux00c>1e-8){
			dlamda1dCe_aa_c = 0.5 + 0.5*(Ce_aa_c - Ce_ss_c)/aux00c;
			dlamda1dCe_ss_c = 0.5 - 0.5*(Ce_aa_c - Ce_ss_c)/aux00c;
			dlamda1dCe_as_c = 2*Ce_as_c/aux00c;
			dlamda0dCe_aa_c = 0.5 - 0.5*(Ce_aa_c - Ce_ss_c)/aux00c;
			dlamda0dCe_ss_c = 0.5 + 0.5*(Ce_aa_c - Ce_ss_c)/aux00c;
			dlamda0dCe_as_c = -2.*Ce_as_c/aux00c;
		}else{
			dlamda1dCe_aa_c = 0.5 ;
			dlamda1dCe_ss_c = 0.5 ;
			dlamda1dCe_as_c = 0;
			dlamda0dCe_aa_c = 0.5;
			dlamda0dCe_ss_c = 0.5;
			dlamda0dCe_as_c = 0;
		}
		dlamda1dCC = dlamda1dCe_aa_c*dCe_aa_cdCC + dlamda1dCe_ss_c*dCe_ss_cdCC + dlamda1dCe_as_c*dCe_as_cdCC;
		dlamda0dCC = dlamda0dCe_aa_c*dCe_aa_cdCC + dlamda0dCe_ss_c*dCe_ss_cdCC + dlamda0dCe_as_c*dCe_as_cdCC;
		//
		if(fabs(lamda1-Ce_aa_c)<1e-8){
			// trouble getting some derivatives, derivative is zero
			dsinVarthetacdlamda1 = 0.0;
			dsinVarthetacdCe_aa_c =0.0;
			dsinVarthetacdCe_as_c =0.0;
			dsinVarthetacdCe_ss_c =0.0;
		}else{
			aux01c = sqrt(Ce_as_c*Ce_as_c + (lamda1-Ce_aa_c)*(lamda1-Ce_aa_c));
			dsinVarthetacdlamda1 = 1./aux01c - (lamda1-Ce_aa_c)/(2*aux01c*aux01c*aux01c)*(2*(lamda1-Ce_aa_c));
			dsinVarthetacdCe_aa_c = -1./aux01c + (lamda1-Ce_aa_c)/(aux01c*aux01c*aux01c)*((lamda1-Ce_aa_c)) ;
			dsinVarthetacdCe_ss_c = 0;
			dsinVarthetacdCe_as_c = -(lamda1-Ce_aa_c)/(aux01c*aux01c*aux01c)*Ce_as_c;
		}
		dsinVarthetacdCC = dsinVarthetacdlamda1*dlamda1dCC + dsinVarthetacdCe_aa_c*dCe_aa_cdCC +dsinVarthetacdCe_ss_c*dCe_ss_cdCC+dsinVarthetacdCe_as_c*dCe_as_cdCC;
		//
		if(fabs(lamda1-Ce_aa_f)<1e-8){
			// trouble getting some derivatives, derivative is zero
			dsinVarthetafdlamda1 = 0.0;
			dsinVarthetafdCe_aa_f = 0.0;
			dsinVarthetafdCe_as_f = 0.0;
			dsinVarthetafdCe_ss_f = 0.0;
		}else{
			aux01f = sqrt(Ce_as_f*Ce_as_f + (lamda1-Ce_aa_f)*(lamda1-Ce_aa_f));
			dsinVarthetafdlamda1 = 1./aux01f - (lamda1-Ce_aa_f)/(2*aux01f*aux01f*aux01f)*(2*(lamda1-Ce_aa_f));
			dsinVarthetafdCe_aa_f = -1./aux01f + (lamda1-Ce_aa_f)/(aux01f*aux01f*aux01f)*((lamda1-Ce_aa_f)) ;
			dsinVarthetafdCe_ss_f = 0;
			dsinVarthetafdCe_as_f = -(lamda1-Ce_aa_f)/(aux01f*aux01f*aux01f)*Ce_as_f;
		}
		dsinVarthetafdCC = dsinVarthetafdlamda1*dlamda1dCC + dsinVarthetafdCe_aa_f*dCe_aa_fdCC +dsinVarthetafdCe_ss_f*dCe_ss_fdCC+dsinVarthetafdCe_as_f*dCe_as_fdCC;
		domegacdCC = ((2.*PIE*dphicdotplusdCC)/(tau_omega_c))*lamda1*sinVartheta_c
				+((2.*PIE*phic_dot_plus)/(tau_omega_c))*dlamda1dCC*sinVartheta_c
				+((2.*PIE*phic_dot_plus)/(tau_omega_c))*lamda1*dsinVarthetacdCC;
		domegafdCC = ((2.*PIE*(dphicdotplusdCC+dphifdotplusdCC))/(tau_omega_f))*lamda1*sinVartheta_f
				+((2.*PIE*(phic_dot_plus+phif_dot_plus))/(tau_omega_f))*dlamda1dCC*sinVartheta_f
				+((2.*PIE*(phic_dot_plus+phif_dot_plus))/(tau_omega_f))*lamda1*dsinVarthetafdCC;
		dRomegacdomegac(0,0) = -sin(omega_c*dt)*dt; dRomegacdomegac(0,1) = -cos(omega_c*dt)*dt;
		dRomegacdomegac(1,0) = cos(omega_c*dt)*dt;  dRomegacdomegac(1,1) = -sin(omega_c*dt)*dt;
		//
		dRomegafdomegaf(0,0) = -sin(omega_f*dt)*dt; dRomegafdomegaf(0,1) = -cos(omega_f*dt)*dt;
		dRomegafdomegaf(1,0) = cos(omega_f*dt)*dt;  dRomegafdomegaf(1,1) = -sin(omega_f*dt)*dt;
		//
		da0c = dRomegacdomegac*a0c_old;
		aux_da0cxdCC = da0c(0)*domegacdCC + Romega_c(0,0)*da0cxdCC + Romega_c(0,1)*da0cydCC;
		aux_da0cydCC = da0c(1)*domegacdCC + Romega_c(1,0)*da0cxdCC + Romega_c(1,1)*da0cydCC;
		da0cxdCC = aux_da0cxdCC;
		da0cydCC = aux_da0cydCC;
		// no reorientation, see before omega = 0
		da0cxdCC.setZero();
		da0cydCC.setZero();
		da0f = dRomegafdomegaf*a0f_old;
		aux_da0fxdCC = da0f(0)*domegafdCC + Romega_f(0,0)*da0fxdCC + Romega_f(0,1)*da0fydCC;
		aux_da0fydCC = da0f(1)*domegafdCC + Romega_f(1,0)*da0fxdCC + Romega_f(1,1)*da0fydCC;
		da0fxdCC = aux_da0fxdCC;
		da0fydCC = aux_da0fydCC;
		da0fxdCC.setZero();
		da0fydCC.setZero();
		// kappa
		dkappacdCC = dkappacdCC + dt*( (dphicdotplusdCC/tau_kappa_c)*( pow(lamda0/lamda1,gamma_kappa_c)/2.  - kappac_old)
						+ (phic_dot_plus/tau_kappa_c)*((gamma_kappa_c/2.)*pow(lamda0/lamda1,gamma_kappa_c-1))*((1./lamda1)*dlamda0dCC-(lamda0/(lamda1*lamda1))*dlamda1dCC));
		dkappafdCC =  dkappafdCC + dt*( ((dphicdotplusdCC+dphifdotplusdCC)/tau_kappa_f)*( pow(lamda0/lamda1,gamma_kappa_f)/2.  -kappaf_old)
						+ ((phic_dot_plus+phif_dot_plus)/tau_kappa_f)*((gamma_kappa_f/2.)*pow(lamda0/lamda1,gamma_kappa_f-1))*((1./lamda1)*dlamda0dCC-(lamda0/(lamda1*lamda1))*dlamda1dCC));
		// march 2021 turned off 
		dkappacdCC.setZero();
		dkappafdCC.setZero();
		// lamdaPa, lamdaPs
		// remember lamdaE_a = sqrt(Ce_aa_a) -> dlamdaE_adCC = 1/(2*lamdaEa)*dCe_aa_cdCC
		dlamdaE_adCC = (1./(2*lamdaE_a))*dCe_aa_cdCC;
		dlamdaE_sdCC = (1./(2*lamdaE_s))*dCe_ss_cdCC;
		//
		if ( lamdaE_a >= sqrt(vartheta_e) )
			dlamdaPadCC = dlamdaPadCC + dt*( dphicdotplusdCC*(lamdaE_a-sqrt(vartheta_e))/tau_lamdaP_a + phic_dot_plus*(dlamdaE_adCC)/tau_lamdaP_a );
		else if ( lamdaE_a <= 1.0 )
			dlamdaPadCC = dlamdaPadCC + dt*( dphicdotplusdCC*(lamdaE_a-1.0)/tau_lamdaP_a + phic_dot_plus*(dlamdaE_adCC)/tau_lamdaP_a );
		// else
		// 	dlamdaPadCC = dlamdaPadCC;
		//
		if ( lamdaE_s >= sqrt(vartheta_e) )
			dlamdaPsdCC = dlamdaPsdCC + dt*( dphicdotplusdCC*(lamdaE_s-sqrt(vartheta_e))/tau_lamdaP_s + phic_dot_plus*(dlamdaE_sdCC)/tau_lamdaP_s );
		else if ( lamdaE_s <= 1.0 )
			dlamdaPsdCC = dlamdaPsdCC + dt*( dphicdotplusdCC*(lamdaE_s-1.0)/tau_lamdaP_s + phic_dot_plus*(dlamdaE_sdCC)/tau_lamdaP_s );
		//
		// dlamdaPadCC = dlamdaPadCC + dt*( dphicdotplusdCC*(lamdaE_a-1)/tau_lamdaP_a + phic_dot_plus*(dlamdaE_adCC)/tau_lamdaP_a
		// 				+ (phic_vol*dphicdotdCC+phif_vol*dphifdotdCC)/thick);
		// dlamdaPsdCC = dlamdaPsdCC + dt*( dphicdotplusdCC*(lamdaE_s-1)/tau_lamdaP_s + phic_dot_plus*(dlamdaE_sdCC)/tau_lamdaP_s
		// 				+ (phic_vol*dphicdotdCC+phif_vol*dphifdotdCC)/thick);
		// turned off
		// dlamdaPadCC.setZero();
		// dlamdaPsdCC.setZero();
		//
		//----------//
		// RHO
		//----------//

		// phi
		dphicdotdrho = (dphicdotplusdrho - dphicdotminusdrho);
		dphicdrho += dt*(dphicdotdrho);
		dphifdotdrho = (dphifdotplusdrho - dphifdotminusdrho);
		dphifdrho += dt*(dphicdotdrho);
		dkcdotdrho = (dkcdotplusdrho - dkcdotminusdrho);
		dkcdrho += dt*(dkcdotdrho);
		dkfdotdrho = (dkfdotplusdrho - dkfdotminusdrho);
		dkfdrho += dt*(dkfdotdrho);

		// a0
		domegacdrho = ((2.*PIE*dphicdotplusdrho)/(tau_omega_c))*lamda1*sinVartheta_c;
		domegafdrho = ((2.*PIE*(dphicdotplusdrho+dphifdotplusdrho))/(tau_omega_f))*lamda1*sinVartheta_f;
		da0cdrho = (dRomegacdomegac*a0c_old)*domegacdrho + Romega_c*da0cdrho;
		da0fdrho = (dRomegafdomegaf*a0f_old)*domegafdrho + Romega_f*da0fdrho;
		da0cdrho.setZero();
		da0fdrho.setZero();

		// kappa
		dkappacdrho += dt*((dphicdotplusdrho/tau_kappa_c)*( pow(lamda0/lamda1,gamma_kappa_c)/2.  -kappac_old));
		dkappafdrho += dt*(((dphicdotplusdrho+dphifdotplusdrho)/tau_kappa_f)*( pow(lamda0/lamda1,gamma_kappa_f)/2.  -kappaf_old));
		dkappacdrho = 0;
		dkappafdrho = 0;

		// lamdaP
		if ( lamdaE_a >= sqrt(vartheta_e) )
			dlamdaPadrho += dt*( dphicdotplusdrho*(lamdaE_a-sqrt(vartheta_e))/tau_lamdaP_a );
		else if ( lamdaE_a <= 1.0 )
			dlamdaPadrho += dt*( dphicdotplusdrho*(lamdaE_a-1.0)/tau_lamdaP_a );
		//
		if ( lamdaE_s >= sqrt(vartheta_e) )
			dlamdaPsdrho += dt*( dphicdotplusdrho*(lamdaE_s-sqrt(vartheta_e))/tau_lamdaP_s );
		else if ( lamdaE_s <= 1.0 )
			dlamdaPsdrho += dt*( dphicdotplusdrho*(lamdaE_s-1.0)/tau_lamdaP_s );
		//
		// dlamdaPadrho += dt*( dphicdotplusdrho*(lamdaE_a-1)/tau_lamdaP_a
		// 	+ (phic_vol*dphicdotdrho+phif_vol*dphifdotdrho)/thick);
		// dlamdaPsdrho += dt*( dphicdotplusdrho*(lamdaE_s-1)/tau_lamdaP_s
		// 	+ (phic_vol*dphicdotdrho+phif_vol*dphifdotdrho)/thick);
		// dlamdaPadrho = 0;
		// dlamdaPsdrho = 0;
		//
		//----------//
		// c
		//----------//

		// phi
		dphicdotdc = (dphicdotplusdc - dphicdotminusdc);
		dphicdc += dt*(dphicdotdc);
		dphifdotdc = (dphifdotplusdc - dphifdotminusdc);
		dphifdc += dt*(dphicdotdc);
		dkcdotdc = (dkcdotplusdc - dkcdotminusdc);
		dkcdc += dt*(dkcdotdc);
		dkfdotdc = (dkfdotplusdc - dkfdotminusdc);
		dkfdc += dt*(dkfdotdc);

		// a0
		domegacdc = ((2.*PIE*dphicdotplusdc)/(tau_omega_c))*lamda1*sinVartheta_c;
		domegafdc = ((2.*PIE*(dphicdotplusdc+dphifdotplusdc))/(tau_omega_f))*lamda1*sinVartheta_f;
		da0cdc = (dRomegacdomegac*a0c_old)*domegacdc + Romega_c*da0cdc;
		da0fdc = (dRomegafdomegaf*a0f_old)*domegafdc + Romega_f*da0fdc;
		da0cdc.setZero();
		da0fdc.setZero();
		// kappa
		dkappacdc += dt*((dphicdotplusdc/tau_kappa_c)*( pow(lamda0/lamda1,gamma_kappa_c)/2.  -kappac_old));
		dkappafdc += dt*(((dphicdotplusdc+dphifdotplusdc)/tau_kappa_f)*( pow(lamda0/lamda1,gamma_kappa_f)/2.  -kappaf_old));
		dkappacdc = 0;
		dkappafdc = 0;
		// lamdaP
		//
		if ( lamdaE_a >= sqrt(vartheta_e) )
			dlamdaPadc += dt*( dphicdotplusdc*(lamdaE_a-sqrt(vartheta_e))/tau_lamdaP_a );
		else if ( lamdaE_a <= 1.0 )
			dlamdaPadc += dt*( dphicdotplusdc*(lamdaE_a-1.0)/tau_lamdaP_a );
		//
		if ( lamdaE_s >= sqrt(vartheta_e) )
			dlamdaPsdc += dt*( dphicdotplusdc*(lamdaE_s-sqrt(vartheta_e))/tau_lamdaP_s );
		else if ( lamdaE_s <= 1.0 )
			dlamdaPsdc += dt*( dphicdotplusdc*(lamdaE_s-1.0)/tau_lamdaP_s );
		//
		// dlamdaPadc += dt*( dphicdotplusdc*(lamdaE_a-1)/tau_lamdaP_a
		// 	+ (phic_vol*dphicdotdc+phif_vol*dphifdotdc)/thick);
		// dlamdaPsdc += dt*( dphicdotplusdc*(lamdaE_s-1)/tau_lamdaP_s
		// 	+ (phic_vol*dphicdotdc+phif_vol*dphifdotdc)/thick);
		// dlamdaPadc = 0;
		// dlamdaPsdc = 0;
	}

	// ASSEMBLE IN ONE VECTOR

	// Mechanics
	dThetadCC(0) = dphicdCC(0,0);
	dThetadCC(1) = dphicdCC(0,1);
	dThetadCC(2) = dphicdCC(0,1);
	dThetadCC(3) = dphicdCC(1,1);

	dThetadCC(4) = dkcdCC(0,0);
	dThetadCC(5) = dkcdCC(0,1);
	dThetadCC(6) = dkcdCC(0,1);
	dThetadCC(7) = dkcdCC(1,1);

	dThetadCC(8)  = da0cxdCC(0,0);
	dThetadCC(9)  = da0cxdCC(0,1);
	dThetadCC(10) = da0cxdCC(0,1);
	dThetadCC(11) = da0cxdCC(1,1);

	dThetadCC(12)  = da0cydCC(0,0);
	dThetadCC(13)  = da0cydCC(0,1);
	dThetadCC(14)  = da0cydCC(0,1);
	dThetadCC(15)  = da0cydCC(1,1);

	dThetadCC(16) = dkappacdCC(0,0);
	dThetadCC(17) = dkappacdCC(0,1);
	dThetadCC(18) = dkappacdCC(0,1);
	dThetadCC(19) = dkappacdCC(1,1);

	//
	dThetadCC(20) = dphifdCC(0,0);
	dThetadCC(21) = dphifdCC(0,1);
	dThetadCC(22) = dphifdCC(0,1);
	dThetadCC(23) = dphifdCC(1,1);

	dThetadCC(24) = dkfdCC(0,0);
	dThetadCC(25) = dkfdCC(0,1);
	dThetadCC(26) = dkfdCC(0,1);
	dThetadCC(27) = dkfdCC(1,1);

	dThetadCC(28)  = da0fxdCC(0,0);
	dThetadCC(29)  = da0fxdCC(0,1);
	dThetadCC(30) = da0fxdCC(0,1);
	dThetadCC(31) = da0fxdCC(1,1);

	dThetadCC(32)  = da0fydCC(0,0);
	dThetadCC(33)  = da0fydCC(0,1);
	dThetadCC(34)  = da0fydCC(0,1);
	dThetadCC(35)  = da0fydCC(1,1);

	dThetadCC(36) = dkappafdCC(0,0);
	dThetadCC(37) = dkappafdCC(0,1);
	dThetadCC(38) = dkappafdCC(0,1);
	dThetadCC(39) = dkappafdCC(1,1);

	//
	dThetadCC(40) = dlamdaPadCC(0,0);
	dThetadCC(41) = dlamdaPadCC(0,1);
	dThetadCC(42) = dlamdaPadCC(0,1);
	dThetadCC(43) = dlamdaPadCC(1,1);

	dThetadCC(44) = dlamdaPsdCC(0,0);
	dThetadCC(45) = dlamdaPsdCC(0,1);
	dThetadCC(46) = dlamdaPsdCC(0,1);
	dThetadCC(47) = dlamdaPsdCC(1,1);

	// RHO
	//
	dThetadrho(0) = dphicdrho;
	dThetadrho(1) = dkcdrho;
	dThetadrho(2) = da0cdrho(0);
	dThetadrho(3) = da0cdrho(1);
	dThetadrho(4) = dkappacdrho;
	//
	dThetadrho(5) = dphifdrho;
	dThetadrho(6) = dkfdrho;
	dThetadrho(7) = da0fdrho(0);
	dThetadrho(8) = da0fdrho(1);
	dThetadrho(9) = dkappafdrho;
	//
	dThetadrho(10) = dlamdaPadrho;
	dThetadrho(11) = dlamdaPsdrho;

	// C
	//
	dThetadc(0) = dphicdc;
	dThetadc(1) = dkcdc;
	dThetadc(2) = da0cdc(0);
	dThetadc(3) = da0cdc(1);
	dThetadc(4) = dkappacdc;
	//
	dThetadc(5) = dphifdc;
	dThetadc(6) = dkfdc;
	dThetadc(7) = da0fdc(0);
	dThetadc(8) = da0fdc(1);
	dThetadc(9) = dkappafdc;
	//
	dThetadc(10) = dlamdaPadc;
	dThetadc(11) = dlamdaPsdc;
}



//--------------------------------------------------------//
// GEOMETRY and ELEMENT ROUTINES
//--------------------------------------------------------//

//-----------------------------//
// Jacobians
//-----------------------------//

Matrix2d evalJacobian(const std::vector<Vector2d> node_X)
{
	// The gradient of the shape functions with respect to the reference coordinates
	// LOOP OVER THE INTEGRATION POINTS
	// shape function and derivatives
    double xi  = 1./3.;
    double eta = 1./3.;
    std::vector<double> R   ={1.-xi-eta,xi,eta};
    std::vector<double> Rxi ={-1.,1.,0.};
    std::vector<double> Reta={-1.,0.,1.};

	// sum over the 3 nodes
	int n_nodes = 3;
	Vector2d dXdxi;dXdxi.setZero();
	Vector2d dXdeta;dXdeta.setZero();
	for(int ni=0;ni<n_nodes;ni++)
	{
		dXdxi += Rxi[ni]*node_X[ni];
		dXdeta += Reta[ni]*node_X[ni];
	}
	// put them in one column
	Matrix2d Jac; Jac<<dXdxi(0),dXdeta(0),dXdxi(1),dXdeta(1);
	// invert and transpose it
	Matrix2d Jac_iT = (Jac.inverse()).transpose();

	return Jac_iT;
}

Matrix2d evalJacobian(const std::vector<Vector2d> node_X, double xi, double eta)
{
	// eval the inverse Jacobian at given xi and eta coordinates
	//
	// eval shape functions
	std::vector<double> R = evalShapeFunctionsR(xi,eta);
	// eval derivatives
	std::vector<double> Rxi = evalShapeFunctionsRxi(xi,eta);
	std::vector<double> Reta = evalShapeFunctionsReta(xi,eta);

	// sum over the 3 nodes
	int n_nodes = 3;
	Vector2d dXdxi;dXdxi.setZero();
	Vector2d dXdeta;dXdeta.setZero();
	for(int ni=0;ni<n_nodes;ni++)
	{
		dXdxi += Rxi[ni]*node_X[ni];
		dXdeta += Reta[ni]*node_X[ni];
	}
	// put them in one column
	Matrix2d Jac; Jac<<dXdxi(0),dXdeta(0),dXdxi(1),dXdeta(1);
	// invert and transpose it
	Matrix2d Jac_iT = (Jac.inverse()).transpose();
	return Jac_iT;
}



//-----------------------------//
// Basis functions
//-----------------------------//

std::vector<double> evalShapeFunctionsR(double xi,double eta)
{
	std::vector<double>  R   ={1.-xi-eta,xi,eta};
	return R;
}
std::vector<double> evalShapeFunctionsRxi(double xi,double eta)
{
	std::vector<double> Rxi={-1.,1.,0.};
	return Rxi;
}
std::vector<double> evalShapeFunctionsReta(double xi,double eta)
{
	std::vector<double> Reta={-1.,0.,1.};
	return Reta;
}

/////////////////////////////////////////////////////////////////////////////////////////
// OTHER (numerical tangent routines and such)
/////////////////////////////////////////////////////////////////////////////////////////

//--------------------------------------------------------//
// RESIDUAL only
//--------------------------------------------------------//
// ELEMENT RESIDUAL
void evalWound(
	double dt,double local_dt,
	const Matrix2d &ip_Jac,
	const std::vector<double> &global_parameters,const std::vector<double> &local_parameters,
	const std::vector<double> &node_rho_0, const std::vector<double> &node_c_0,const std::vector<double> &node_p_0,
	const double &ip_phic_0,const double &ip_mu_0,const double &ip_kc_0,const Vector2d &ip_a0c_0,const double &ip_kappac_0,
	const double &ip_phif_0,const double &ip_kf_0,const Vector2d &ip_a0f_0,const double &ip_kappaf_0,
	const Vector2d &ip_lamdaP_0,
	const std::vector<double> &node_rho, const std::vector<double> &node_c,
	double &ip_phic, double &ip_mu, double &ip_kc, Vector2d &ip_a0c, double &ip_kappac,
	double &ip_phif, double &ip_kf, Vector2d &ip_a0f, double &ip_kappaf,
	Vector2d &ip_lamdaP,
	const std::vector<Vector2d> &node_x,
	VectorXd &Re_x, VectorXd &Re_rho, VectorXd &Re_c)
{

	//---------------------------------//
	// INPUT
	//  dt: time step
	//  local_dt: local time step for remodeling
	//	elem_jac_IP: jacobians at the integration points, needed for the deformation grad
	//  global_param: material parameters for global newton
	//  local_param: material parameters for local newton
	//  Xi_t: global fields at previous time step
	//  Theta_t: structural fields at previous time steps
	//  Xi: current guess of the global fields
	//  Theta: current guess of the structural fields
	//	node_x: deformed positions
	//
	// OUTPUT
	//  Re: all residuals
	//  Ke: all tangents
	//
	// Algorithm
	//  0. Loop over integration points
	//	1. F,rho,c,nabla_rho,nabla_c: deformation at IP
	//  2. LOCAL NEWTON -> update the current guess of the structural parameters
	//  3. Fe,Fp
	//	4. Se_pas,Se_act,S
	//	5. Qrho,Srho,Qc,Sc
	//  6. Residuals
	//  7. Tangents
	//---------------------------------//



	//---------------------------------//
	// GLOBAL PARAMETERS
	//
	// double k0 = global_parameters[0]; // neo hookean
	double k2c = global_parameters[1]; // nonlinear exponential collagen
	double k2f = global_parameters[2]; // nonlinear exponential fibronectin
	double t_rho = global_parameters[3]; // force of fibroblasts
	double t_rho_c = global_parameters[4]; // force of myofibroblasts enhanced by chemical
	double K_t_c = global_parameters[5]; // saturation of chemical on force
	double D_rhorho = global_parameters[6]; // diffusion of cells
	double D_rhoc = global_parameters[7]; // diffusion of chemotactic gradient
	double Fc_max = global_parameters[8]; // effect on chemical on the amount of chemotaxis bias
	double D_cc = global_parameters[9]; // diffusion of chemical
	double p_rho =global_parameters[10]; // production of fibroblasts naturally
	double p_rho_c = global_parameters[11]; // production enhanced by the chem
	double p_rho_theta = global_parameters[12]; // mechanosensing
	double K_rho_c= global_parameters[13]; // saturation of cell production by chemical
	double K_rho_rho = global_parameters[14]; // saturation of cell by cell
	double d_rho = global_parameters[15] ;// decay of cells
	double theta_phy = global_parameters[16]; // physiological state of area stretch
	double gamma_c_thetaE = global_parameters[17]; // sensitivity of heviside function
	double p_c_rho = global_parameters[18];// production of C by cells
	double p_c_thetaE = global_parameters[19]; // coupling of elastic and chemical
	double K_c_c = global_parameters[20];// saturation of chem by chem
	double d_c = global_parameters[21]; // decay of chemical
	double thick_0 = global_parameters[22]; // thickness in reference
	double D_precprec = global_parameters[23]; // diffusion of neutrophil/pdgf
	double d_prec = global_parameters[24]; // decay of neutrophil/pdgf
	double p_c_prec = global_parameters[25]; // production of TGFb1/Macrophage by neutrophil/PDGF
	double prec_healthy =  global_parameters[26]; // healthy level of precursor 
	double c_target =  global_parameters[27]; // target level of TGFb1 to ensure that healthy value = 1
	double rho_healthy =  global_parameters[28]; // healthy level of rho 
	//
	//
	//---------------------------------//



	//---------------------------------//
	// GLOBAL VARIABLES
	// Initialize the residuals to zero and declare some global stuff
	Re_x.setZero();
	Re_rho.setZero();
	Re_c.setZero();
	int n_nodes = 3;
	double load = 1;
	std::vector<Vector2d> Ebasis; Ebasis.clear();
	Matrix2d Rot90;Rot90<<0.,-1.,1.,0.;
	Ebasis.push_back(Vector2d(1.,0.)); Ebasis.push_back(Vector2d(0.,1.));
	//---------------------------------//



	//--------------------------------------//
	// LOOP OVER INTEGRATION POINTS, only 1!
	//--------------------------------------//

	// array with integration points
	// shape function and derivatives
    double xi  = 1./3.;
    double eta = 1./3.;
    std::vector<double> R   ={1.-xi-eta,xi,eta};
    std::vector<double> Rxi ={-1.,1.,0.};
    std::vector<double> Reta={-1.,0.,1.};
	double Jac = 1./ip_Jac.determinant();

	// declare variables and gradients at IP
	std::vector<Vector2d> dRdXi;dRdXi.clear();
	Vector2d dxdxi,dxdeta;
	dxdxi.setZero();dxdeta.setZero();
	double rho_0=0.; Vector2d drho0dXi; drho0dXi.setZero();
	double rho=0.; Vector2d drhodXi; drhodXi.setZero();
	double c_0=0.; Vector2d dc0dXi; dc0dXi.setZero();
	double c=0.; Vector2d dcdXi; dcdXi.setZero();
	double prec=0;
	//
	for(int ni=0;ni<n_nodes;ni++)
	{
		dRdXi.push_back(Vector2d(Rxi[ni],Reta[ni]));

		dxdxi += node_x[ni]*Rxi[ni];
		dxdeta += node_x[ni]*Reta[ni];

		rho_0 += node_rho_0[ni]*R[ni];
		drho0dXi(0) += node_rho_0[ni]*Rxi[ni];
		drho0dXi(1) += node_rho_0[ni]*Reta[ni];

		rho += node_rho[ni]*R[ni];
		drhodXi(0) += node_rho[ni]*Rxi[ni];
		drhodXi(1) += node_rho[ni]*Reta[ni];

		c_0 += node_c_0[ni]*R[ni];
		dc0dXi(0) += node_c_0[ni]*Rxi[ni];
		dc0dXi(1) += node_c_0[ni]*Reta[ni];

		c += node_c[ni]*R[ni];
		dcdXi(0) += node_c[ni]*Rxi[ni];
		dcdXi(1) += node_c[ni]*Reta[ni];

		prec += node_p_0[ni]*R[ni];
	}
	//
	//---------------------------------//



	//---------------------------------//
	// EVAL GRADIENTS
	//
	// Deformation gradient and strain
	// assemble the columns
	Matrix2d dxdXi; dxdXi<<dxdxi(0),dxdeta(0),dxdxi(1),dxdeta(1);
	// F = dxdX
	Matrix2d FF = dxdXi*ip_Jac.transpose();
	// the strain
	Matrix2d Identity;Identity<<1,0,0,1;
	// Matrix2d EE = 0.5*(FF.transpose()*FF - Identity);
	Matrix2d CC = FF.transpose()*FF;
	Matrix2d CCinv = CC.inverse();
	//
	// Gradient of concentrations in current configuration
	Matrix2d dXidx = dxdXi.inverse();
	Vector2d grad_rho0 = dXidx.transpose()*drho0dXi;
	Vector2d grad_rho  = dXidx.transpose()*drhodXi;
	Vector2d grad_c0   = dXidx.transpose()*dc0dXi;
	Vector2d grad_c    = dXidx.transpose()*dcdXi;
	//
	// Gradient of concentrations in reference
	Vector2d Grad_rho0 = ip_Jac*drho0dXi;
	Vector2d Grad_rho = ip_Jac*drhodXi;
	Vector2d Grad_c0 = ip_Jac*dc0dXi;
	Vector2d Grad_c = ip_Jac*dcdXi;
	//
	// Gradient of basis functions for the nodes in reference
	std::vector<Vector2d> Grad_R;Grad_R.clear();
	Grad_R.push_back(ip_Jac*dRdXi[0]);
	Grad_R.push_back(ip_Jac*dRdXi[1]);
	Grad_R.push_back(ip_Jac*dRdXi[2]);
	//
	// Gradient of basis functions in deformed configuration
	std::vector<Vector2d> grad_R;grad_R.clear();
	grad_R.push_back(dXidx.transpose()*dRdXi[0]);
	grad_R.push_back(dXidx.transpose()*dRdXi[1]);
	grad_R.push_back(dXidx.transpose()*dRdXi[2]);
	//
	//---------------------------------//

	//---------------------------------//
	// LOCAL NEWTON: structural problem
	//
	VectorXd dThetadCC(12*4);dThetadCC.setZero();
	VectorXd dThetadrho(12);dThetadrho.setZero();
	VectorXd dThetadc(12);dThetadc.setZero();
	//*******//
	localWoundProblem(dt,local_dt,local_parameters,c,prec,rho,CC,
			ip_phic_0,ip_mu_0,ip_kc_0,ip_a0c_0,ip_kappac_0,
			ip_phif_0,ip_kf_0,ip_a0f_0,ip_kappaf_0,
			ip_lamdaP_0,
			ip_phic,ip_mu,ip_kc,ip_a0c,ip_kappac,
			ip_phif,ip_kf,ip_a0f,ip_kappaf,
			ip_lamdaP,
			dThetadCC,dThetadrho,dThetadc);
	//*******//
	//
	// rename variables to make it easier
	double phic_0 = ip_phic_0;
	double mu_0 = ip_mu_0;
	double kc_0 = ip_kc_0;
	Vector2d a0c_0 = ip_a0c_0;
	double kappac_0 = ip_kappac_0;
	//
	double phif_0 = ip_phif_0;
	double kf_0 = ip_kf_0;
	Vector2d a0f_0 = ip_a0f_0;
	double kappaf_0 = ip_kappaf_0;
	//
	Vector2d lamdaP_0 = ip_lamdaP_0;
	//
	double phic = ip_phic;
	double mu = ip_mu;
	double kc = ip_kc;
	Vector2d a0c = ip_a0c;
	double kappac = ip_kappac;
	//
	double phif = ip_phif;
	double kf = ip_kf;
	Vector2d a0f = ip_a0f;
	double kappaf = ip_kappaf;
	//
	Vector2d lamdaP = ip_lamdaP;
	//
	double lamdaP_a_0 = lamdaP_0(0);
	double lamdaP_s_0 = lamdaP_0(1);
	double lamdaP_a = lamdaP(0);
	double lamdaP_s = lamdaP(1);
	double thetaP = lamdaP_a*lamdaP_s;
	//
	// make sure the update preserved length
	double norma0c = sqrt(a0c.dot(a0c));
	if(fabs(norma0c-1.)>0.001){std::cout<<"update did not preserve unit length of a0c\n";}
	ip_a0c = a0c/(sqrt(a0c.dot(a0c)));
	a0c = a0c/(sqrt(a0c.dot(a0c)));
	double norma0f = sqrt(a0f.dot(a0f));
	if(fabs(norma0f-1.)>0.001){std::cout<<"update did not preserve unit length of a0f\n";}
	ip_a0f = a0f/(sqrt(a0f.dot(a0f)));
	a0f = a0f/(sqrt(a0f.dot(a0f)));
	//

	//---------------------------------//
	// CALCULATE SOURCE AND FLUX
	//
	Matrix2d SS_pas = evalSS_pas(FF,rho,Grad_rho,c,Grad_c,phic,mu,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,global_parameters);
	//
	//*************************************************************//
	// magnitude from systems bio
	// and derivatives as well
	Matrix2d SS_act;SS_act.setZero();
	Matrix3d DDact;DDact.setZero();
	Matrix2d dSSactdrho_explicit;dSSactdrho_explicit.setZero();
	Matrix2d dSSactdc_explicit;dSSactdc_explicit.setZero();
	// pass current deformation and all structures
	evalSS_act(FF,rho,Grad_rho,c,Grad_c,phic,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,global_parameters,load,
		SS_act,DDact,dSSactdrho_explicit,dSSactdc_explicit);
	//*************************************************************//

	Vector3d SS_voigt;
	SS_voigt(0) = SS_act(0,0)+SS_pas(0,0);
	SS_voigt(1) = SS_act(1,1)+SS_pas(1,1);
	SS_voigt(2) = SS_act(0,1)+SS_pas(0,1);

	//*************************************************************//
	// Flux and Source terms for the rho and the C
	Vector2d Q_rho,dQ_rhodrho_explicit,dQ_rhodc_explicit;
	Matrix2d dQ_rhoxdCC_explicit,dQ_rhoydCC_explicit,dQ_rhodGradrho,dQ_rhodGradc;
	Vector2d Q_c,dQ_cdrho_explicit,dQ_cdc_explicit;
	Matrix2d dQ_cxdCC_explicit,dQ_cydCC_explicit,dQ_cdGradrho,dQ_cdGradc;
	//
	evalQ_rho(FF,rho,Grad_rho,c,Grad_c,phic,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,global_parameters,
		Q_rho,dQ_rhoxdCC_explicit,dQ_rhoydCC_explicit,dQ_rhodrho_explicit,dQ_rhodGradrho,dQ_rhodc_explicit,dQ_rhodGradc);
	evalQ_c(FF,rho,Grad_rho,c,Grad_c,phic,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,global_parameters,
		Q_c,dQ_cxdCC_explicit,dQ_cydCC_explicit,dQ_cdrho_explicit,dQ_cdGradrho,dQ_cdc_explicit,dQ_cdGradc);

	//
	double S_rho, dSrhodrho_explicit, dSrhodc_explicit;
	Matrix2d dSrhodCC_explicit;
	double S_c, dScdrho_explicit, dScdc_explicit;
	Matrix2d dScdCC_explicit;
	//
	evalS_rho(FF,rho,Grad_rho,c,Grad_c,phic,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,global_parameters,
		S_rho,dSrhodCC_explicit,dSrhodrho_explicit,dSrhodc_explicit);
	//
	evalS_c(FF,rho,Grad_rho,c,Grad_c,prec,phic,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,global_parameters,
		S_c,dScdCC_explicit,dScdrho_explicit,dScdc_explicit);
	//*************************************************************//

	//---------------------------------//

	//---------------------------------//
	// ADD TO THE RESIDUAL
	//
	Matrix2d deltaFF,deltaCC;
	Vector3d deltaCC_voigt;
	for(int nodei=0;nodei<3;nodei++){
		for(int coordi=0;coordi<2;coordi++){
			// alternatively, define the deltaCC
			deltaFF = Ebasis[coordi]*Grad_R[nodei].transpose();
			deltaCC = deltaFF.transpose()*FF + FF.transpose()*deltaFF;
			deltaCC_voigt = Vector3d(deltaCC(0,0),deltaCC(1,1),2.*deltaCC(1,0));
			Re_x(nodei*2+coordi) += 0.5*Jac*SS_voigt.dot(deltaCC_voigt)/2.;
		}
	}
	//
	Re_rho(0) += Jac*(((rho-rho_0)/dt - S_rho)*thetaP*R[0] - Grad_R[0].dot(Q_rho))*thick_0/2;
	Re_rho(1) += Jac*(((rho-rho_0)/dt - S_rho)*thetaP*R[1] - Grad_R[1].dot(Q_rho))*thick_0/2;
	Re_rho(2) += Jac*(((rho-rho_0)/dt - S_rho)*thetaP*R[2] - Grad_R[2].dot(Q_rho))*thick_0/2;
	//
	Re_c(0) += Jac*(((c-c_0)/dt - S_c)*thetaP*R[0] - Grad_R[0].dot(Q_c))*thick_0/2;
	Re_c(1) += Jac*(((c-c_0)/dt - S_c)*thetaP*R[1] - Grad_R[1].dot(Q_c))*thick_0/2;
	Re_c(2) += Jac*(((c-c_0)/dt - S_c)*thetaP*R[2] - Grad_R[2].dot(Q_c))*thick_0/2;
	//
}


//-------------------------------------------------------------------------------------//
// POST PROCESSING
//-------------------------------------------------------------------------------------//

void evalDeformationGradient(const Matrix2d &ip_Jac, const std::vector<Vector2d> &node_x, Matrix2d &ip_FF)
{
	int n_nodes = 3;
	// shape function and derivatives
    double xi  = 1./3.;
    double eta = 1./3.;
    std::vector<double> R   ={1.-xi-eta,xi,eta};
    std::vector<double> Rxi ={-1.,1.,0.};
    std::vector<double> Reta={-1.,0.,1.};
	double Jac = 1./ip_Jac.determinant();

	// declare variables and gradients at IP
	std::vector<Vector2d> dRdXi;dRdXi.clear();
	Vector2d dxdxi,dxdeta;
	dxdxi.setZero();dxdeta.setZero();

	//
	for(int ni=0;ni<n_nodes;ni++)
	{
		dRdXi.push_back(Vector2d(Rxi[ni],Reta[ni]));

		dxdxi += node_x[ni]*Rxi[ni];
		dxdeta += node_x[ni]*Reta[ni];
	}

	// Deformation gradient and strain
	// assemble the columns
	Matrix2d dxdXi; dxdXi<<dxdxi(0),dxdeta(0),dxdxi(1),dxdeta(1);
	// F = dxdX
	ip_FF = dxdXi*ip_Jac.transpose();
} 

void evalStrainsStress(
	const Matrix2d &ip_Jac,
	const std::vector<double> &global_parameters,const std::vector<double> &local_parameters,
	const std::vector<double> &node_rho_0, const std::vector<double> &node_c_0,
	double ip_phic_0,double ip_mu_0,double ip_kc_0,const Vector2d ip_a0c_0,double ip_kappac_0,
	double ip_phif_0,double ip_kf_0,const Vector2d &ip_a0f_0,double ip_kappaf_0,
	const Vector2d &ip_lamdaP_0,
	const std::vector<Vector2d> &node_x,
	Vector2d &EE_eiv, Vector2d &SS_eiv, Matrix2d &FF, Matrix2d &sigma)
{
	int n_nodes = 3;
	std::vector<Vector2d> Ebasis; Ebasis.clear();
	Matrix2d Rot90;Rot90<<0.,-1.,1.,0.;
	Ebasis.push_back(Vector2d(1.,0.)); Ebasis.push_back(Vector2d(0.,1.));
	//---------------------------------//
	//--------------------------------------//
	// LOOP OVER INTEGRATION POINTS, only 1!
	//--------------------------------------//

	// array with integration points
	// shape function and derivatives
    double xi  = 1./3.;
    double eta = 1./3.;
    std::vector<double> R   ={1.-xi-eta,xi,eta};
    std::vector<double> Rxi ={-1.,1.,0.};
    std::vector<double> Reta={-1.,0.,1.};
	double Jac = 1./ip_Jac.determinant();

	// declare variables and gradients at IP
	std::vector<Vector2d> dRdXi;dRdXi.clear();
	Vector2d dxdxi,dxdeta;
	dxdxi.setZero();dxdeta.setZero();
	double rho=0.; Vector2d drhodXi; drhodXi.setZero();
	double c=0.; Vector2d dcdXi; dcdXi.setZero();
	//
	for(int ni=0;ni<n_nodes;ni++)
	{
		dRdXi.push_back(Vector2d(Rxi[ni],Reta[ni]));

		dxdxi += node_x[ni]*Rxi[ni];
		dxdeta += node_x[ni]*Reta[ni];

		rho += node_rho_0[ni]*R[ni];
		drhodXi(0) += node_rho_0[ni]*Rxi[ni];
		drhodXi(1) += node_rho_0[ni]*Reta[ni];

		c += node_c_0[ni]*R[ni];
		dcdXi(0) += node_c_0[ni]*Rxi[ni];
		dcdXi(1) += node_c_0[ni]*Reta[ni];
	}
	//
	//---------------------------------//


	//---------------------------------//
	// EVAL GRADIENTS
	//
	// Deformation gradient and strain
	// assemble the columns
	Matrix2d dxdXi; dxdXi<<dxdxi(0),dxdeta(0),dxdxi(1),dxdeta(1);
	// F = dxdX
	FF = dxdXi*ip_Jac.transpose();
	// the strain
	Matrix2d Identity;Identity<<1,0,0,1;
	Matrix2d EE = 0.5*(FF.transpose()*FF - Identity);
	Matrix2d CC = FF.transpose()*FF;
	Matrix2d CCinv = CC.inverse();
	//
	// Gradient of concentrations in current configuration
	Matrix2d dXidx = dxdXi.inverse();
	Vector2d grad_rho  = dXidx.transpose()*drhodXi;
	Vector2d grad_c    = dXidx.transpose()*dcdXi;
	//
	// Gradient of concentrations in reference
	Vector2d Grad_rho = ip_Jac*drhodXi;
	Vector2d Grad_c = ip_Jac*dcdXi;
	//
	// Gradient of basis functions for the nodes in reference
	std::vector<Vector2d> Grad_R;Grad_R.clear();
	Grad_R.push_back(ip_Jac*dRdXi[0]);
	Grad_R.push_back(ip_Jac*dRdXi[1]);
	Grad_R.push_back(ip_Jac*dRdXi[2]);
	//
	// Gradient of basis functions in deformed configuration
	std::vector<Vector2d> grad_R;grad_R.clear();
	grad_R.push_back(dXidx.transpose()*dRdXi[0]);
	grad_R.push_back(dXidx.transpose()*dRdXi[1]);
	grad_R.push_back(dXidx.transpose()*dRdXi[2]);

	// rename variables to make it easier
	double phic = ip_phic_0;
	double mu = ip_mu_0;
	double kc = ip_kc_0;
	Vector2d a0c = ip_a0c_0;
	double kappac = ip_kappac_0;
	//
	double phif = ip_phif_0;
	double kf = ip_kf_0;
	Vector2d a0f = ip_a0f_0;
	double kappaf = ip_kappaf_0;
	//
	Vector2d lamdaP = ip_lamdaP_0;
	//
	double lamdaP_a = lamdaP(0);
	double lamdaP_s = lamdaP(1);
	double thetaP = lamdaP_a*lamdaP_s;
	//

	//---------------------------------//
	// CALCULATE passive stress
	//
	Matrix2d SS_pas = evalSS_pas(FF,rho,Grad_rho,c,Grad_c,phic,mu,kc,a0c,kappac,phif,kf,a0f,kappaf,lamdaP,global_parameters);
	//
	//---------------------------------//

	// Get principal directions for the strains
	EigenSolver<Matrix2d> es(CC);
	// then polar decomposition is
	std::vector<Vector2d> Ubasis(2,Vector2d(0.0,0.0));
	Matrix2d CC_eiv = es.eigenvectors().real();
	Ubasis[0] =  CC_eiv.col(0);
	Ubasis[1] =  CC_eiv.col(1);
	Vector2d Clamda =  es.eigenvalues().real();
	// Build the U mnatrix from basus and the root of the iegenvuales
	Matrix2d UU = sqrt(Clamda(0))*Ubasis[0]*Ubasis[0].transpose() + sqrt(Clamda(1))*Ubasis[1]*Ubasis[1].transpose() ;
	Matrix2d RR = FF*UU.inverse();

	// STORE in the output
	EE_eiv(0) = 0.5*(Clamda(0)-1);
	EE_eiv(1) = 0.5*(Clamda(1)-1);

	// get the eigenvectors for the cauchy stress
	// from second piola to cuachy
	sigma = FF*SS_pas*FF.transpose();
	EigenSolver<Matrix2d> es2(sigma);
	// then polar decomposition is
	std::vector<Vector2d> sig_basis(2,Vector2d(0.0,0.0));
	Matrix2d sigma_eiv = es2.eigenvectors().real();
	sig_basis[0] =  sigma_eiv.col(0);
	sig_basis[1] =  sigma_eiv.col(1);
	Vector2d sigma_lamda =  es2.eigenvalues().real();

	// STORE in output vector
	SS_eiv(0) = sigma_lamda(0);
	SS_eiv(1) = sigma_lamda(1);

}

void evalPrec(double dt,
	const std::vector<double> &node_p_0,const std::vector<double> &node_p, const std::vector<Vector2d> &node_x,
	const Vector2d &ip_lamdaP,const std::vector<double> &global_parameters,const Matrix2d &ip_Jac,
	Vector3d &Re_p, Matrix3d &Ke_p_p)
{
	double thick_0 = global_parameters[22];
	double D_precprec = global_parameters[23]; // diffusion of neutrophil/pdgf
	double d_prec  = global_parameters[24]; // decay  of neutrophil/pdgf

	Re_p.setZero();
	Ke_p_p.setZero();
	// shape function and derivatives
    double xi  = 1./3.;
    double eta = 1./3.;
    std::vector<double> R   ={1.-xi-eta,xi,eta};
    std::vector<double> Rxi ={-1.,1.,0.};
    std::vector<double> Reta={-1.,0.,1.};
	double Jac = 1./ip_Jac.determinant();
	// declare variables and gradients at IP
	std::vector<Vector2d> dRdXi;dRdXi.clear();
	Vector2d dxdxi,dxdeta;
	dxdxi.setZero();dxdeta.setZero();
	double p_0=0.; Vector2d dp0dXi; dp0dXi.setZero();
	double p=0.; Vector2d dpdXi; dpdXi.setZero();
	int n_nodes = 3;
	for(int ni=0;ni<n_nodes;ni++)
	{
		dRdXi.push_back(Vector2d(Rxi[ni],Reta[ni]));

		dxdxi += node_x[ni]*Rxi[ni];
		dxdeta += node_x[ni]*Reta[ni];

		p_0 += node_p_0[ni]*R[ni];
		dp0dXi(0) += node_p_0[ni]*Rxi[ni];
		dp0dXi(1) += node_p_0[ni]*Reta[ni];

		p += node_p[ni]*R[ni];
		dpdXi(0) += node_p[ni]*Rxi[ni];
		dpdXi(1) += node_p[ni]*Reta[ni];
	}
	//---------------------------------//
	// EVAL GRADIENTS
	//
	// Deformation gradient and strain
	// assemble the columns
	Matrix2d dxdXi; dxdXi<<dxdxi(0),dxdeta(0),dxdxi(1),dxdeta(1);
	// F = dxdX
	Matrix2d FF = dxdXi*ip_Jac.transpose();
	// the strain
	Matrix2d Identity;Identity<<1,0,0,1;
	// Matrix2d EE = 0.5*(FF.transpose()*FF - Identity);
	Matrix2d CC = FF.transpose()*FF;
	Matrix2d CCinv = CC.inverse();
	//
	// Gradient of concentrations in current configuration
	Matrix2d dXidx = dxdXi.inverse();
	Vector2d grad_p0 = dXidx.transpose()*dp0dXi;
	Vector2d grad_p  = dXidx.transpose()*dpdXi;
	// Gradient of concentrations in reference
	Vector2d Grad_p0 = ip_Jac*dp0dXi;
	Vector2d Grad_p = ip_Jac*dpdXi;
	// Gradient of basis functions 
	std::vector<Vector2d> Grad_R;Grad_R.clear();
	Grad_R.push_back(ip_Jac*dRdXi[0]);
	Grad_R.push_back(ip_Jac*dRdXi[1]);
	Grad_R.push_back(ip_Jac*dRdXi[2]);
	// Preprocessing
	double thetaP = ip_lamdaP(0)*ip_lamdaP(1);
	Vector2d Q_p = -thetaP*D_precprec*CCinv*Grad_p;
	Matrix2d dQ_pdGradp = -thetaP*D_precprec*CCinv;

	Re_p(0) += Jac*(((p-p_0)/dt + d_prec*p)*thetaP*R[0] - Grad_R[0].dot(Q_p))*thick_0/2;
	Re_p(1) += Jac*(((p-p_0)/dt + d_prec*p)*thetaP*R[1] - Grad_R[1].dot(Q_p))*thick_0/2;
	Re_p(2) += Jac*(((p-p_0)/dt + d_prec*p)*thetaP*R[2] - Grad_R[2].dot(Q_p))*thick_0/2;

	for(int nodei=0;nodei<3;nodei++){
		for(int nodej=0;nodej<3;nodej++){
			Ke_p_p(nodei,nodej) += Jac*thick_0/2*(thetaP*R[nodei]*R[nodej]/dt + thetaP* R[nodei]*d_prec*R[nodej] -1.* Grad_R[nodei].dot(dQ_pdGradp*Grad_R[nodej]));
		}
	}	
}

